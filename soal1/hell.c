#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <b64/cdecode.h>
#include <ctype.h>

#define MAX_PATH 256
#define PASSWORD "12345"

void reverseFilename(char *filename) {
    int length = strlen(filename);
    for (int i = 0; i < length / 2; i++) {
        char temp = filename[i];
        filename[i] = filename[length - i - 1];
        filename[length - i - 1] = temp;
    }
}

void moveAndReverse(char *sourcePath, char *destPath) {
    if (rename(sourcePath, destPath) != 0) {
        perror("Error renaming file");
    }

    char *filename = strrchr(sourcePath, '/');
    if (filename != NULL) {
        filename++; 
        reverseFilename(filename);
    }
}

void moveAndDelete(char *sourcePath, char *destPath) {
    if (rename(sourcePath, destPath) != 0) {
        perror("Error renaming file");
    }

    if (remove(sourcePath) != 0) {
        perror("Error removing file");
    }
}

void processGalleryFolder(char *galleryPath) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir(galleryPath)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) { 
                char sourcePath[MAX_PATH];
                snprintf(sourcePath, sizeof(sourcePath), "%s/%s", galleryPath, entry->d_name);

                if (strncmp(entry->d_name, "rev.", 4) == 0) {
                    moveAndReverse(sourcePath, galleryPath);
                } 
                else if (strncmp(entry->d_name, "delete.", 7) == 0) {
                    moveAndDelete(sourcePath, galleryPath);
                }
            }
        }
        closedir(dir);
    } else {
        perror("Error opening directory");
    }
}

void processSisopFolder(char *sisopPath) {
    char scriptPath[MAX_PATH];
    snprintf(scriptPath, sizeof(scriptPath), "%s/script.sh", sisopPath);

    chmod(scriptPath, S_IRUSR | S_IWUSR | S_IXUSR); 

    FILE *testFile = fopen("test-file.txt", "w");
    if (testFile != NULL) {
        fprintf(testFile, "This is a test file.");
        fclose(testFile);
    }
}

void processTulisanFolder(char *tulisanPath) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir(tulisanPath)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) { 
                char filePath[MAX_PATH];
                snprintf(filePath, sizeof(filePath), "%s/%s", tulisanPath, entry->d_name);

                if (strncmp(entry->d_name, "base64", 6) == 0) {
                    decodeBase64File(filePath);
                } else if (strncmp(entry->d_name, "rot13", 5) == 0) {
                    decodeROT13File(filePath);
                } else if (strncmp(entry->d_name, "hex", 3) == 0) {
                    decodeHexFile(filePath);
                } else if (strncmp(entry->d_name, "rev", 3) == 0) {
                    decodeReverseFile(filePath);
                }
            }
        }
        closedir(dir);
    } else {
        perror("Error opening directory");
    }
}

void processDisableArea(char *disableAreaPath) {
    char password[MAX_PATH];
    printf("Enter password for access to disable-area: ");
    scanf("%s", password);

    if (strcmp(password, PASSWORD) == 0) {
        printf("Access granted!\n");

    } else {
        printf("Access denied!\n");
    }
}

void decodeBase64File(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    base64_decodestate state;
    base64_init_decodestate(&state);

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead, bytesDecoded;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        bytesDecoded = base64_decode_block(inputBuffer, bytesRead, outputBuffer, &state);
        fwrite(outputBuffer, 1, bytesDecoded, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("Base64 decoding completed.\n");
}

void decodeROT13File(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output_rot13.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        for (size_t i = 0; i < bytesRead; i++) {
            char c = inputBuffer[i];

            if (isalpha(c)) {
                char base = islower(c) ? 'a' : 'A';
                outputBuffer[i] = base + (c - base + 13) % 26;
            } else {
                outputBuffer[i] = c;
            }
        }

        fwrite(outputBuffer, 1, bytesRead, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("ROT13 decoding completed.\n");
}

void decodeHexFile(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output_hex.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        for (size_t i = 0; i < bytesRead; i += 2) {
            char hexByte[3] = {inputBuffer[i], inputBuffer[i + 1], '\0'};
            outputBuffer[i / 2] = (char)strtol(hexByte, NULL, 16);
        }

        fwrite(outputBuffer, 1, bytesRead / 2, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("Hex decoding completed.\n");
}

void decodeReverseFile(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output_reverse.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        for (size_t i = 0; i < bytesRead; i++) {
            outputBuffer[bytesRead - i - 1] = inputBuffer[i];
        }

        fwrite(outputBuffer, 1, bytesRead, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("Reverse decoding completed.\n");
}

int main() {
    char galleryPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/gallery";
    char sisopPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/sisop";
    char tulisanPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/tulisan";
    char disableAreaPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/disable-area";

    processGalleryFolder(galleryPath);

    processSisopFolder(sisopPath);

    processTulisanFolder(tulisanPath);

    processDisableArea(disableAreaPath);

    return 0;
}