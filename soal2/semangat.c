#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h> 
#include <attr/xattr.h>


static const char *dirpath = "/home/caca/soal_2";
static const char *log_path = "/home/caca/soal_2/logs-fuse.log"; 

void log_event(const char *tag, const char *path, const char *info, int success) {
    time_t now;
    struct tm *tm_info;
    char timestamp[20];

    time(&now); 
    tm_info = localtime(&now); //mendapatkan waktu saat ini
    strftime(timestamp, 20, "%d/%m/%Y-%H:%M:%S", tm_info); //format waktu

    FILE *log_file = fopen(log_path, "a"); //file log dibuka di mode append
    if (log_file) {
        fprintf(log_file, "[%s]::%s::%s::%s::%s\n", (success ? "SUCCESS" : "FAILED"), timestamp, tag, path, info); //detail pencatatan log
        fclose(log_file); 
    }
}

static void generate_html(const char *csv_path) {
    FILE *csv_file = fopen(csv_path, "r"); //membuka file csv
    if (!csv_file) {
        perror("Failed to open CSV file");
        return;
    }

    char line[256];
    int firstLine = 1;  //menandai lagi di baris 1 atau tidak

    while (fgets(line, sizeof(line), csv_file)) {
        if (firstLine) {
            firstLine = 0;
            continue;  //skip baris pertama (format)
        }

        char *file, *title, *body;  //breakdown line untuk baca koma
        file = strtok(line, ",");
        title = strtok(NULL, ",");
        body = strtok(NULL, ",");

        if (file && title && body) {
            //format html
            char html_content[512];
            snprintf(html_content, sizeof(html_content),
                     "<!DOCTYPE html>\n"
                     "<html lang=\"en\">\n"
                     "<head>\n"
                     "<meta charset=\"UTF-8\" />\n"
                     "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n"
                     "<title>%s</title>\n"
                     "</head>\n"
                     "<body>\n"
                     "%s"
                     "</body>\n"
                     "</html>\n",
                     title, body);

            //membuat dan menulis file html ke dalam folder website
            char html_path[1000];
            snprintf(html_path, sizeof(html_path), "%s/website/%s.html", dirpath, file);
            FILE *html_file = fopen(html_path, "w");
            if (html_file) {
                fprintf(html_file, "%s", html_content);
                fclose(html_file);
                printf("HTML file generated: %s\n", html_path);
                log_event("generate_html", html_path, "HTML file generated", 1);
            } else {
                perror("Failed to create HTML file");
                log_event("generate_html", html_path, "Failed to generate HTML file", 0);
            }
        }
    }

    fclose(csv_file);
}


static int xmp_open(const char *path, struct fuse_file_info *fi) { //untuk membuka file
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags); //membuka file
    if (res == -1) {
        log_event("open", path, "Failed to open file", 0);
        return -errno;
    }

    fi->fh = res;

    log_event("open", path, "Open file", 1);
    return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi) { //fungsi close file
    (void) path;
    int res = close(fi->fh);
    if (res == -1) {
        log_event("release", path, "Failed to close file", 0);
        return -errno;
    }

    log_event("release", path, "Close file", 1);
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //fungsi untuk menulis file (nano)
    (void) path;

    int res = pwrite(fi->fh, buf, size, offset); //menulis isi file
    if (res == -1) {
        log_event("write", path, "Failed to write to file", 0);
        return -errno;
    }

    char fpath[1000]; //diperiksa apakah lagi di directory website
    sprintf(fpath, "%s%s", dirpath, path);

    if (strstr(path, "/website/") != NULL && strstr(path, ".csv") != NULL) {
        generate_html(fpath); //membuat file html berdasarkan isi csv
    }

    log_event("write", path, "Write to file", 1);
    return res;
}


static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) { //fungsi untuk bikin file baru
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags | O_CREAT, mode);
    if (res == -1) {
        log_event("create", path, "Failed to create file", 0);
        return -errno;
    }

    fi->fh = res;

    log_event("create", path, "Create file", 1);

    if (strstr(path, "/website/") != NULL && strstr(path, ".csv") != NULL) {
        generate_html(fpath); //menampilkan file html berdasarkan isi csv
    }

    return 0;
}

static int xmp_unlink(const char *path) { //fungsi untuk hapus file (rm)
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    const char *filename = strrchr(path, '/') + 1; //mengambil nama file dari path

    if (strncmp(filename, "restricted", 10) == 0) {  //meriksa apakah nama file dimulai dengan "restricted"
        log_event("unlink", path, "Failed to remove file - Restricted", 0);
        return -EACCES; //akses ditolak untuk file yang dimulai dengan "restricted"
    }

    //lanjutkan dengan penghapusan file jika bukan restricted
    int res = unlink(fpath);
    if (res == -1) {
        log_event("unlink", path, "Failed to remove file", 0);
        return -errno;
    }

    log_event("unlink", path, "Removed file", 1);
    return 0;
}



static int xmp_getattr(const char *path, struct stat *stbuf) { //fungsi untuk nampilin atribut file
    int res;
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path); //mengambil path lengkap

    res = lstat(fpath, stbuf); //mengambil info atribut

    if (res == -1) {
        log_event("getattr", path, "Failed to get attributes", 0);
        return -errno;
    }

    log_event("getattr", path, "Get attributes", 1);
    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) { //baca isi dircetory
    char fpath[1000];

    if (strcmp(path, "/") == 0) { //cek path
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath); //buka dir

    if (dp == NULL) {
        log_event("readdir", path, "Failed to read directory", 0);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0) break;
    }

    closedir(dp);

    log_event("readdir", path, "Read directory", 1);
    return 0;
}


static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //fungsi untuk ngebaca isi file
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    
    if (strstr(fpath, "/text/") != NULL) { //cek apakah lagi di riectory text
        char user_password[100];
        printf("Enter the password: ");
        scanf("%s", user_password);

        //membaca password di file password.bin
        char stored_passwords[1000]; 
        char password_path[1000];
        sprintf(password_path, "%s/password.bin", dirpath);

        FILE *password_file = fopen(password_path, "r");
        if (password_file) {
            fscanf(password_file, "%s", stored_passwords);
            fclose(password_file);

            
            char *stored_password = strtok(stored_passwords, ","); //breakdown line password di password.bin
            while (stored_password != NULL) {
                
                if (strcmp(user_password, stored_password) == 0) { //cek apakah sesuai dengan password di password.bin
                    break; //memperbolehkan akses
                }
                stored_password = strtok(NULL, ",");
            }

            
            if (stored_password == NULL) { //jika password tidak sesuai
                printf("Invalid password\n");
                log_event("read", path, "Invalid password", 0);
                return -EACCES; //melarang akses
            }
        } else {
            printf("Password file not found\n");
            log_event("read", path, "Password file not found", 0);
            return -ENOENT; 
        }
    }

    int res = 0;
    int fd = 0;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) {
        log_event("read", path, "Failed to open file", 0);
        return -errno;
    }

    res = pread(fd, buf, size, offset);

    if (res == -1) {
        log_event("read", path, "Failed to read file", 0);
        res = -errno;
    }

    close(fd);

    log_event("read", path, "Read file", 1);
    return res;
}

static int move_files_to_dir(const char *src_dir, const char *dest_dir, const char *extension, const char *path) { //fungsi untuk memindahkan file
    struct dirent *entry;
    DIR *dir = opendir(src_dir);

    if (dir == NULL) {
        perror("opendir");
        return -1;
    }

    while ((entry = readdir(dir)) != NULL) { //iterasi melalui file pada direktori sumber,
        if (entry->d_type == DT_REG) { //cek jika entry adalah file regular
            const char *file_ext = strrchr(entry->d_name, '.'); //mendapatkan ekstensi file
            if (file_ext && strcmp(file_ext, extension) == 0) { //cocokkan dengan ekstensi yang diinginkan
                char src_path[1000]; 
                char dest_path[1000];
                sprintf(src_path, "%s/%s", src_dir, entry->d_name); //path sumber
                sprintf(dest_path, "%s/%s/%s", dirpath, dest_dir, entry->d_name); //path tujuan

                if (rename(src_path, dest_path) == -1) { //pemindahan file
                    perror("rename");
                    log_event("move_files_to_dir", path, "Failed to move file", 0);
                    return -1;
                }

                printf("File moved: %s -> %s\n", src_path, dest_path);
                log_event("move_files_to_dir", path, "File moved", 1);
            }
        }
    }

    closedir(dir);
    return 0;
}

static int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) { //fungsi untuk menetapkan atribut
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lsetxattr(fpath, name, value, size, flags);

    if (res == -1) {
        log_event("setxattr", path, "Failed to set extended attribute", 0);
        return -errno;
    }

    log_event("setxattr", path, "Set extended attribute", 1);
    return 0;
}

static int xmp_listxattr(const char *path, char *list, size_t size) { //mendaptakan daftar atribut
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = llistxattr(fpath, list, size);

    if (res == -1) {
        log_event("listxattr", path, "Failed to list extended attributes", 0);
        return -errno;
    }

    log_event("listxattr", path, "List extended attributes", 1);
    return res;
}


static int xmp_getxattr(const char *path, const char *name, char *value, size_t size) { //mendapatkan nilai dari daftar atribut
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lgetxattr(fpath, name, value, size);

    if (res == -1) {
        log_event("getxattr", path, "Failed to get extended attribute", 0);
        return -errno;
    }

    log_event("getxattr", path, "Get extended attribute", 1);
    return res;
}

static int xmp_mkdir(const char *path, mode_t mode) //fungsi untuk membuat directory dengan path sesuai
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);
    if (res == -1) {
        res = -errno;
        perror("mkdir");
        log_event("mkdir", path, "Failed to create directory", 0);
        return res;
    }

    //memindahkan file dengan format tertentu ke directory yang sesuai
    if (strcmp(path, "/documents") == 0) {
        move_files_to_dir(dirpath, "documents", ".pdf", path);
        move_files_to_dir(dirpath, "documents", ".docx", path);
    } else if (strcmp(path, "/images") == 0) {
        move_files_to_dir(dirpath, "images", ".jpg", path);
        move_files_to_dir(dirpath, "images", ".png", path);
        move_files_to_dir(dirpath, "images", ".ico", path);
    } else if (strcmp(path, "/website") == 0) {
        move_files_to_dir(dirpath, "website", ".js", path);
        move_files_to_dir(dirpath, "website", ".html", path);
        move_files_to_dir(dirpath, "website", ".json", path);
    } else if (strcmp(path, "/sisop") == 0) {
        move_files_to_dir(dirpath, "sisop", ".c", path);
        move_files_to_dir(dirpath, "sisop", ".sh", path);
    } else if (strcmp(path, "/text") == 0) {
        move_files_to_dir(dirpath, "text", ".txt", path);
    } else if (strcmp(path, "/ai") == 0) {
        move_files_to_dir(dirpath, "ai", ".ipynb", path);
        move_files_to_dir(dirpath, "ai", ".csv", path);
    }
    if (res == 0) {
        log_event("mkdir", path, "Directory created", 1);
    }
    return 0;
}

static int xmp_rmdir(const char *path) { //fungsi untuk menghapus directory
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    const char *dirname = strrchr(path, '/') + 1;     //ambil nama direktori dari path

    if (strncmp(dirname, "restricted", 10) == 0) {
        log_event("rmdir", path, "Failed to remove directory - Restricted", 0);
        return -EACCES; //akses ditolak untuk direktori yang dimulai dengan "restricted"
    }

    int res = rmdir(fpath);
    if (res == -1) {
        log_event("rmdir", path, "Failed to remove directory", 0);
        return -errno;
    }

    log_event("rmdir", path, "Removed directory", 1);
    return 0;
}

//inisialisasi operasi fuse
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .open = xmp_open,
    .release = xmp_release,
    .write = xmp_write,
    .create = xmp_create,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
};

int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(log_path, "w"); //membuat file log kalau gaada
    if (log_file) {
        fclose(log_file);
    }

    return fuse_main(argc, argv, &xmp_oper, NULL); //mulai fuse
}
