#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <zlib.h>

int main() {
    //membaca password terenkripsi dari file zip-pass.txt
    FILE* file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        printf("Gagal membaca file zip-pass.txt\n");
        return 1;
    }
    
    char encryptedPassword[128];
    if (fgets(encryptedPassword, sizeof(encryptedPassword), file) == NULL) {
        printf("Gagal membaca password terenkripsi\n");
        fclose(file);
        return 1;
    }
    
    fclose(file);

    //mendekripsi password base64
    BIO* bio, *b64;
    char password[128];
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(encryptedPassword, -1);
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    int decryptedLength = BIO_read(bio, password, sizeof(password));

    if (decryptedLength <= 0) {
        printf("Gagal mendekripsi password base64\n");
        BIO_free_all(bio);
        return 1;
    }
    
    BIO_free_all(bio);
    password[decryptedLength] = '\0'; //menambahkan null-terminator
    
    printf("Password hasil dekripsi: %s\n", password);

  
    return 0;
}

