#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <ctype.h>


#define PORT 8080

void sendToClient(const char *message, int clientSocket) { //mengirim pesan socker ke client
    send(clientSocket, message, strlen(message), 0);
}

void addNewEntry(const char *day, const char *genre, const char *title) { //menambah entry baru ke webtoon.csv
    FILE *file = fopen("webtoon.csv", "a"); //membuka file dalam mode append (tambahan)
    if (file == NULL) {
        printf("Error opening the file.\n");
        return;
    }

    fprintf(file, "%s,%s,%s", day, genre, title); //menulis entry baru
    fclose(file);
}
void deleteByTitle(const char *title) { //menghapus line berdasarkan judul
    FILE *file = fopen("webtoon.csv", "r");
    FILE *tempFile = fopen("temp.csv", "w");

    if (file == NULL || tempFile == NULL) {
        printf("Error opening the file.\n");
        return;
    }

    char line[256];

    while (fgets(line, sizeof(line), file)) { //membaca baris tiap file
        char *copyLine = strdup(line); //duplikasi baris untuk breakdown token
        char *day = strtok(copyLine, ",");
        char *genre = strtok(NULL, ",");
        char *titleToken = strtok(NULL, ",");

        while (isspace(*titleToken)) { //menghilangkan spasi judul
            titleToken++;
        }

        if (strcmp(titleToken, title) != 0) { //jika judul tidak sesuai entry, tulis baris ke file temp
            fprintf(tempFile, "%s", line);
        }
        free(copyLine);
    }

    fclose(file);
    fclose(tempFile);

    remove("webtoon.csv"); //hapus file asli
    rename("temp.csv", "webtoon.csv"); //rename temp jadi webtoon.csv
}

void sendAllTitles(FILE *file, int clientSocket) { //mengirim semua judul ke client
    char line[256];
    char sendBuffer[1024] = {0};
    int count = 1;

    while (fgets(line, sizeof(line), file)) {
        strtok(line, ","); //memproses bagian pertama (hari)
        strtok(NULL, ","); //memproses bagian kedua (genre)
        char *title = strtok(NULL, ","); //mengambil judul dari line

        char tempBuffer[256] = {0}; //buffer sementara untuk tiap judul
        sprintf(tempBuffer, "%d. %s\n", count, title);
        strcat(sendBuffer, tempBuffer);
        count++;
    }

    sendToClient(sendBuffer, clientSocket); //mengirim judul ke client
}

void sendByGenre(const char *genre, FILE *file, int clientSocket) { //mengirim judul berdasarkan genre 
    char line[256];
    char sendBuffer[1024] = {0};
    int count = 1;

    while (fgets(line, sizeof(line), file)) { //membaca tiap baris di file
       
        strtok(line, ","); //melewati nilai koma pertama (hari)
        
    	
        char *genreInFile = strtok(NULL, ","); //ambil genre setelah koma pertama
        char *title = strtok(NULL, ","); //ambil judul

        //menghilangkan spasi
        char formattedGenre[20];
        strncpy(formattedGenre, genre, sizeof(formattedGenre));
        formattedGenre[strcspn(formattedGenre, "\r\n")] = 0; 

        if (strcmp(genreInFile, formattedGenre) == 0) { //jika genre cocok sesuai input
            char tempBuffer[256] = {0};
            sprintf(tempBuffer, "%d. %s\n", count, title); //format nomor dan judul
            strcat(sendBuffer, tempBuffer); //menambahkan judul ke buffer pengiriman
            count++;
        }
    }

    if (count > 1) {
        sendToClient(sendBuffer, clientSocket); //mengirim jduul ke client
    } else {
        sendToClient("No titles found for the given genre.\n", clientSocket);
    }
}



void sendByDay(const char *day, FILE *file, int clientSocket) { //membaca judul berdasarkan hari
    char line[256];
    char sendBuffer[1024] = {0};
    int count = 1;

    while (fgets(line, sizeof(line), file)) { //membaca tiap baris dalam file
        char *dayInFile = strtok(line, ","); //membaca hari dari baris
        char *genre = strtok(NULL, ","); //lewati bagian genre
        char *title = strtok(NULL, ","); //ambil judul dari baris

        //menghilangkan spasi di awal dan akhir dari string hari
        char formattedDay[20];
        strncpy(formattedDay, day, sizeof(formattedDay));
        formattedDay[strcspn(formattedDay, "\r\n")] = 0; //menghilangkan karakter newline jika ada

        if (strcmp(dayInFile, formattedDay) == 0) { //jika input hari cocok
            char tempBuffer[256] = {0};
            sprintf(tempBuffer, "%d. %s\n", count, title);
            strcat(sendBuffer, tempBuffer); //menambahkan judul ke buffer pengiriman
            count++;
        }
    }

    if (count > 1) {
        sendToClient(sendBuffer, clientSocket); //mengirim judul file
    } else {
        sendToClient("No titles found for the given day.\n", clientSocket);
    }
}


int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) { ///buat socket untuk server
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) { //mengikat socket ke port 
        perror("Binding failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) { //mendengar koneksi dari client
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) { //menerima koneksi baru
        perror("Accept failed");
        exit(EXIT_FAILURE);
    }

    FILE *file = fopen("webtoon.csv", "r"); //membuka file webtoon.csv dalam mode baca
    if (file == NULL) {
        printf("Error opening the file.\n");
        return -1;
    }

    while (1) {
        char input[100] = {0};
        int valread = read(new_socket, input, 100);
        printf("Received: %s\n", input);

        if (strstr(input, "hari") != NULL) { //menampilkan judul berdasarkan hari
            char *day = strchr(input, ' ') + 1;
            sendByDay(day, file, new_socket);
        } else if (strstr(input, "genre") != NULL) { //menampilkan judul berdasarkan genre
            char *genre = strchr(input, ' ') + 1;
            sendByGenre(genre, file, new_socket);
        } else if (strstr(input, "all") != NULL) { //menampilkan seluruh judul
            sendAllTitles(file, new_socket);
        } else if (strstr(input, "add") != NULL) { //menambahkan line baru
            sendToClient("Input the day, genre, and title separated by commas (e.g., Monday,Action,Title):\n", new_socket);
            
            char addInfo[256] = {0};
            int valread = read(new_socket, addInfo, 256);
            
            char *day = strtok(addInfo, ",");
            char *genre = strtok(NULL, ",");
            char *title = strtok(NULL, ",");
            
            if (day != NULL && genre != NULL && title != NULL) {
                addNewEntry(day, genre, title);
                sendToClient("New entry added to webtoon.csv\n", new_socket);
        }} else if (strstr(input, "delete") != NULL) { //menghapus line sesuai judul
            sendToClient("Input the title to delete:\n", new_socket);

            char titleToDelete[256] = {0};
            int valread = read(new_socket, titleToDelete, 256);

            deleteByTitle(titleToDelete);
            sendToClient("Entry deleted from webtoon.csv\n", new_socket);
        } else { //jika input client tidak dipahami
            sendToClient("Invalid Command\n", new_socket); 
        }

        fseek(file, 0, SEEK_SET);
    }
 


    fclose(file);
    close(new_socket);
    close(server_fd);

    return 0;
}
