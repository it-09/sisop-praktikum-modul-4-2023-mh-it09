# *LAPORAN RESMI PRAKTIKUM SISTEM OPERASI MODUL 4*
Berikut adalah Laporan Resmi Praktikum Sistem Operasi Modul 4 oleh Kelompok IT 09.

---
## *TABLE OF CONTENT*

 - [Soal 1](#soal1)
 - [Soal 2](#soal2)
 - [Soal 3](#soal3)
 
---

### **I. SOAL 1<a name="soal1"></a>**
**A. PEMBAHASAN**

1. Pada folder Gallery, kita diharuskan membuat folder dengan prefix "rev." dan membuat folder dengan prefix "delete." Dalam folder prefix "rev", setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama. Lalu dalam folder prefix "delete", setiap gambar yang dipindahkan ke dalamnya akan mengalami penghapusan file secara otomatis. Kemudian dalam folder Sisop, kita diharuskan untuk mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan". Lalu kita juga harus membuat prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut. Kemudian pada folder Tulisan, kita harus mengelola berkas-berkas teks dengan menggunakan fuse Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64. Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13. Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya. Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya. Lalu jika seseorang ingin mengakses folder dan file pada “disable-area”, kita harus memasukkan sebuah password. Dan terakhir kita juga harus membuat logs-fuse.log agar setiap proses yang dilakukan akan tercatat dengan format `[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]`

2. Ini adalah program `hell.c` yang saya buat pertama kali. Kode ini masih belum sempurna karena output yang belum sesuai dengan yang dimintai oleh soal.

```c
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <b64/cdecode.h>
#include <ctype.h>

#define MAX_PATH 1024
#define PASSWORD "12345"

void reverseFilename(char *filename);
void moveAndReverse(char *sourcePath, char *destPath);
void moveAndDelete(char *sourcePath, char *destPath);
void processGalleryFolder(char *galleryPath);
void processSisopFolder(char *sisopPath);
void processTulisanFolder(char *tulisanPath);
void processDisableArea(char *disableAreaPath);
void decodeBase64File(char *filePath);
void decodeROT13File(char *filePath);
void decodeHexFile(char *filePath);
void decodeReverseFile(char *filePath);

int main() {
    char galleryPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/gallery";
    char sisopPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/sisop";
    char tulisanPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/tulisan";
    char disableAreaPath[] = "/home/agas_linux/SISOP_MODUL_4.1/data/disable-area";

    processGalleryFolder(galleryPath);
    processSisopFolder(sisopPath);
    processTulisanFolder(tulisanPath);
    processDisableArea(disableAreaPath);

    struct dirent *entry;
    char filePath[MAX_PATH];
    DIR *dir;

    if ((dir = opendir(tulisanPath)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            snprintf(filePath, sizeof(filePath), "%s/%s", tulisanPath, entry->d_name);

            if (strncmp(entry->d_name, "hex", 3) == 0) {
                decodeHexFile(filePath);
            } else if (strncmp(entry->d_name, "rev", 3) == 0) {
                decodeReverseFile(filePath);
            }
        }
        closedir(dir);
    } else {
        perror("Error opening directory");
    }

    return 0;
}

void reverseFilename(char *filename) {
    int length = strlen(filename);
    for (int i = 0; i < length / 2; i++) {
        char temp = filename[i];
        filename[i] = filename[length - i - 1];
        filename[length - i - 1] = temp;
    }
}

void moveAndReverse(char *sourcePath, char *destPath) {
    if (rename(sourcePath, destPath) != 0) {
        perror("Error renaming file");
    }

    char *filename = strrchr(sourcePath, '/');
    if (filename != NULL) {
        filename++; 
        reverseFilename(filename);
    }
}

void moveAndDelete(char *sourcePath, char *destPath) {
    if (rename(sourcePath, destPath) != 0) {
        perror("Error renaming file");
    }

    if (remove(sourcePath) != 0) {
        perror("Error removing file");
    }
}

void processGalleryFolder(char *galleryPath) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir(galleryPath)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) { 
                char sourcePath[MAX_PATH];
                snprintf(sourcePath, sizeof(sourcePath), "%s/%s", galleryPath, entry->d_name);

                if (strncmp(entry->d_name, "rev.", 4) == 0) {
                    moveAndReverse(sourcePath, galleryPath);
                } 
                else if (strncmp(entry->d_name, "delete.", 7) == 0) {
                    moveAndDelete(sourcePath, galleryPath);
                }
            }
        }
        closedir(dir);
    } else {
        perror("Error opening directory");
    }
}

void processSisopFolder(char *sisopPath) {
    char scriptPath[MAX_PATH];
    snprintf(scriptPath, sizeof(scriptPath), "%s/script.sh", sisopPath);

    chmod(scriptPath, S_IRUSR | S_IWUSR | S_IXUSR); 

    FILE *testFile = fopen("test-file.txt", "w");
    if (testFile != NULL) {
        fprintf(testFile, "This is a test file.");
        fclose(testFile);
    }
}

void processTulisanFolder(char *tulisanPath) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir(tulisanPath)) != NULL) {
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) { 
                char filePath[MAX_PATH];
                snprintf(filePath, sizeof(filePath), "%s/%s", tulisanPath, entry->d_name);

                if (strncmp(entry->d_name, "base64", 6) == 0) {
                    decodeBase64File(filePath);
                } else if (strncmp(entry->d_name, "rot13", 5) == 0) {
                    decodeROT13File(filePath);
                } else if (strncmp(entry->d_name, "hex", 3) == 0) {
                    decodeHexFile(filePath);
                } else if (strncmp(entry->d_name, "rev", 3) == 0) {
                    decodeReverseFile(filePath);
                }
            }
        }
        closedir(dir);
    } else {
        perror("Error opening directory");
    }
}

void processDisableArea(char *disableAreaPath) {
    char password[MAX_PATH];
    printf("Enter password for access to disable-area: ");
    scanf("%s", password);

    if (strcmp(password, PASSWORD) == 0) {
        printf("Access granted!\n");

    } else {
        printf("Access denied!\n");
    }
}

void decodeBase64File(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    base64_decodestate state;
    base64_init_decodestate(&state);

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead, bytesDecoded;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        bytesDecoded = base64_decode_block(inputBuffer, bytesRead, outputBuffer, &state);
        fwrite(outputBuffer, 1, bytesDecoded, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("Base64 decoding completed.\n");
}

void decodeROT13File(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output_rot13.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        for (size_t i = 0; i < bytesRead; i++) {
            char c = inputBuffer[i];

            if (isalpha(c)) {
                char base = islower(c) ? 'a' : 'A';
                outputBuffer[i] = base + (c - base + 13) % 26;
            } else {
                outputBuffer[i] = c;
            }
        }

        fwrite(outputBuffer, 1, bytesRead, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("ROT13 decoding completed.\n");
}

void decodeHexFile(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output_hex.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        for (size_t i = 0; i < bytesRead; i += 2) {
            char hexByte[3] = {inputBuffer[i], inputBuffer[i + 1], '\0'};
            outputBuffer[i / 2] = (char)strtol(hexByte, NULL, 16);
        }

        fwrite(outputBuffer, 1, bytesRead / 2, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("Hex decoding completed.\n");
}

void decodeReverseFile(char *filePath) {
    FILE *inputFile = fopen(filePath, "r");
    if (inputFile == NULL) {
        perror("Error opening file for decoding");
        return;
    }

    FILE *outputFile = fopen("decoded_output_reverse.txt", "w");
    if (outputFile == NULL) {
        perror("Error opening output file for decoding");
        fclose(inputFile);
        return;
    }

    const int bufferSize = 4096;
    char inputBuffer[bufferSize];
    char outputBuffer[bufferSize];

    size_t bytesRead;

    while ((bytesRead = fread(inputBuffer, 1, bufferSize, inputFile)) > 0) {
        for (size_t i = 0; i < bytesRead; i++) {
            outputBuffer[bytesRead - i - 1] = inputBuffer[i];
        }

        fwrite(outputBuffer, 1, bytesRead, outputFile);
    }

    fclose(inputFile);
    fclose(outputFile);

    printf("Reverse decoding completed.\n");
}
```

3. Ini adalah output dari program tersebut. Terlihat bahwa output dari programnya masih belum sesuai dengan yang diinginkannya. Misalnya pada folder Gallery, ketika saya memindahkan salah satu gambar ke folder `rev`, nama file masih belum ter-reversed dengan baik. Begitu pula pada folder lainnya, ketika saya mencoba melakukan perintah sesuai soal, itu masih belum sempurna berhasil.


![output_hell.c_pertama](/uploads/65edf25af9d30b614fed7cd6bc57a300/output_hell.c_pertama.PNG)


**B. REVISI*
1. Setelah mengetahui bahwa kode program `hell.c` masih belum sempurna, maka saya melakukan perombakan kode seperti berikut:

```c
#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h> 
#include <openssl/evp.h>

static const char *dirpath = "/home/asnan_linux/SISOP_MODUL_4.4";
static const char *correctPassword = "12345";

static void log_info(const char *tag, const char *information, int success) {
    time_t now;
    struct tm timestamp;
    char time_buffer[20];

    time(&now);
    localtime_r(&now, &timestamp);
    strftime(time_buffer, sizeof(time_buffer), "%d/%m/%Y-%H:%M:%S", &timestamp);

    FILE *log_file = fopen("/home/asnan_linux/SISOP_MODUL_4.4/logs-fuse.log", "a");
    if (log_file == NULL) {
        log_file = fopen("/home/asnan_linux/SISOP_MODUL_4.4/logs-fuse.log", "w");
    }

    if (log_file != NULL) {
        if (success) {
            fprintf(log_file, "[SUCCESS]::%s::%s::%s\n", time_buffer, tag, information);
        } else {
            fprintf(log_file, "[FAILED]::%s::%s::%s\n", time_buffer, tag, information);
        }
        fclose(log_file);
    }
}



void reverseString(char *str, int start, int end)
{
    while (start < end)
    {
        // Swap the characters at start and end
        char temp = str[start];
        str[start] = str[end];
        str[end] = temp;

        // Move to the next pair of characters
        start++;
        end--;
    }
}


void reverse_decode(char *input, size_t length) {
    reverseString(input, 0, length - 1);
}

int isInsideDisableArea(const char *path)
{
    const char *disableAreaPath = "/disable-area";

    // Check if the path starts with "/disable-area"
    return strncmp(path, disableAreaPath, strlen(disableAreaPath)) == 0;
}



// Function to check if a path is within the "gallery" folder
int isInsideGallery(const char *path)
{
    const char *galleryPath = "/gallery";

    // Check if the path starts with "/gallery"
    return strncmp(path, galleryPath, strlen(galleryPath)) == 0;
}

char* base64_decode(const char* input, size_t length) {
    BIO *b64, *bmem;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bmem = BIO_new_mem_buf((void*)input, length);
    bmem = BIO_push(b64, bmem);

    char *buffer = (char *)malloc(length);
    if (!buffer) {
        BIO_free_all(bmem);
        return NULL;
    }

    int decoded_size = BIO_read(bmem, buffer, length);
    if (decoded_size < 0) {
        free(buffer);
        BIO_free_all(bmem);
        return NULL;
    }

    // Optional: Trim the buffer to the actual size of the decoded data
    char *trimmed_buffer = (char *)realloc(buffer, decoded_size + 1);
    if (!trimmed_buffer) {
        // Handle realloc failure, if needed
        free(buffer);
        BIO_free_all(bmem);
        return NULL;
    }

    trimmed_buffer[decoded_size] = '\0';
    BIO_free_all(bmem);

    return trimmed_buffer;
}

void rot13_decode(char* input, size_t length) {
    for (size_t i = 0; i < length; ++i) {
        if ((input[i] >= 'A' && input[i] <= 'Z') || (input[i] >= 'a' && input[i] <= 'z')) {
            char base = (input[i] >= 'A' && input[i] <= 'Z') ? 'A' : 'a';
            input[i] = (input[i] - base + 13) % 26 + base;
        }
    }
}

char* hex_decode(const char* input, size_t length) {
    size_t i, j;
    char *output = (char *)malloc(length / 2 + 1);

    for (i = j = 0; i < length; i += 2) {
        sscanf(input + i, "%2hhx", (unsigned char *)(output + j));
        j++;
    }

    output[j] = '\0';
    return output;
}


static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1) {
        log_info("getattr", "Failed to get attributes", 0);
       
        return -errno;
    }

    log_info("getattr", "Attributes retrieved successfully", 1);

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    // Check if the directory is inside the "disable-area" folder
    if (strncmp(path, "/disable-area", 13) == 0)
    {
        char enteredPassword[100];
        printf("Enter password: ");
        fflush(stdout);
        scanf("%s", enteredPassword);

        // Check the entered password
        if (strcmp(enteredPassword, correctPassword) != 0)
        {
            printf("Invalid password!\n");
            log_info("readdir", "Invalid password", 0);
            return -EACCES;
        }
        log_info("readdir", "Password is correct", 1); // Log success
    }

    dp = opendir(fpath);

    if (dp == NULL) {
        log_info("readdir", "Failed to read directory", 0);
        return -errno;
    }

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        char fullpath[1500];  // Increased buffer size

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        snprintf(fullpath, sizeof(fullpath), "%s/%s", fpath, de->d_name);

        res = filler(buf, de->d_name, &st, 0);

        if (res != 0) {
            log_info("readdir", "Failed to fill directory", 0);
            break;
        }
    }

    closedir(dp);
    log_info("readdir", "Directory read successfully", 1);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    // Check if the file is in the "tulisan" folder
    if (strstr(path, "/tulisan/") != NULL)
    {
        fd = open(fpath, O_RDONLY);

        if (fd == -1)
        {
            log_info("read", "Failed to open file", 0);
            return -errno;
        }

        // If the file name contains the word "halo", decode the content with Base64 using EVP
        const char *filename = strrchr(path, '/') + 1;
        if (strstr(filename, "base64") != NULL)
        {
            char *encoded_content;
            size_t encoded_size;

            // Read the original encoded content
            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            // Decode the content with Base64 using EVP
            encoded_content = base64_decode(buf, res);
            if (!encoded_content)
            {
                close(fd);
                log_info("read", "Failed to decode Base64", 0);
                return -ENOMEM;
            }
            encoded_size = strlen(encoded_content);

            // Adjust the size to fit the decoded content
            size = (size < encoded_size - offset) ? size : (encoded_size - offset);

            // Copy the decoded content into the buffer
            memcpy(buf, encoded_content + offset, size);

            // Clean up
            free(encoded_content);
            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
        else if (strstr(filename, "rot13") != NULL)
        {
            // If the file name contains the word "rot13", decode the content with ROT13
            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            // Decode the content with ROT13
            rot13_decode(buf, res);

            // Adjust the size to fit the decoded content
            size = (size < res - offset) ? size : (res - offset);

            // Copy the decoded content into the buffer
            memcpy(buf, buf + offset, size);

            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
        else if (strstr(filename, "hex") != NULL)
        {
            // If the file name contains the word "hex", decode the content from hexadecimal representation
            char *hex_content;
            size_t hex_size;

            // Read the original hex content
            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            // Decode the content from hexadecimal representation
            hex_content = hex_decode(buf, res);
            if (!hex_content)
            {
                close(fd);
                log_info("read", "Failed to decode hex", 0);
                return -ENOMEM;
            }
            hex_size = strlen(hex_content);

            // Adjust the size to fit the decoded content
            size = (size < hex_size - offset) ? size : (hex_size - offset);

            // Copy the decoded content into the buffer
            memcpy(buf, hex_content + offset, size);

            // Clean up
            free(hex_content);
            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
        else if (strstr(filename, "rev") != NULL)
        {
            // If the file name contains the word "rev", decode the content by reversing the text
            res = pread(fd, buf, size, offset);

            if (res == -1)
            {
                close(fd);
                log_info("read", "Failed to read file content", 0);
                return -errno;
            }

            // Decode the content by reversing the text
            reverse_decode(buf, res);

            // Adjust the size to fit the decoded content
            size = (size < res - offset) ? size : (res - offset);

            // Copy the decoded content into the buffer
            memcpy(buf, buf + offset, size);

            close(fd);

            log_info("read", "Read and decoded file content successfully", 1);
            return size;
        }
    }
    else
    {
        fd = open(fpath, O_RDONLY);

        if (fd == -1)
        {
            log_info("read", "Failed to open file", 0);
            return -errno;
        }
    }

    // If the file is not in the "tulisan" folder, read the original content
    res = pread(fd, buf, size, offset);

    if (res == -1)
    {
        res = -errno;
        log_info("read", "Failed to read file content", 0);
    }

    close(fd);

    log_info("read", "Read file content successfully", 1);
    return res;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);

    if (res == -1)
    {
        log_info("mkdir", "Failed to create directory", 0);
        return -errno;
    }

    log_info("mkdir", "Directory created successfully", 1);

    return 0;
}


static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    int fd;
    sprintf(fpath, "%s%s", dirpath, path);

    int res;
    (void) fi;

    res = creat(fpath, mode);

    if (res == -1)
    {
        log_info("create", "Failed to create file", 0);
        return -errno;
    }

    close(res);

    // Reverse the content of the file if it has the prefix "test"
    if (strncmp(path, "/sisop/test", 11) == 0)
    {
        fd = open(fpath, O_RDWR);

        if (fd == -1)
        {
            log_info("create", "Failed to open file", 0);
            return -errno;
        }

        char buffer[1000];
        ssize_t bytesRead = read(fd, buffer, sizeof(buffer));

        if (bytesRead > 0)
        {
            // Reverse the content
            int start = 0;
            int end = bytesRead - 1;
            reverseString(buffer, start, end);

            // Move to the beginning of the file and write the reversed content
            lseek(fd, 0, SEEK_SET);
            write(fd, buffer, bytesRead);
        }

        close(fd);

        log_info("create", "File created and content reversed successfully", 1);
        return 0;
    }

    log_info("create", "File created successfully", 1);
    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    // Check if the file is inside the "disable-area" folder
    if (strncmp(path, "/disable-area", 13) == 0)
    {
        char enteredPassword[100];
        printf("Enter password: ");
        fflush(stdout);
        scanf("%s", enteredPassword);

        // Check the entered password
        if (strcmp(enteredPassword, correctPassword) != 0)
        {
            printf("Invalid password!\n");
            log_info("open", "Invalid password", 0);
            return -EACCES;
        }
    }

    log_info("open", "File opened successfully", 1);
    return 0;
}


static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    int fd;
    int res;

    (void)fi;

    sprintf(fpath, "%s%s", dirpath, path);

    fd = open(fpath, O_WRONLY);

    if (fd == -1)
    {
        log_info("write", "Failed to open file", 0);
        return -errno;
    }

    // Reverse the content of the file if it has the prefix "test"
    if (strncmp(path, "/sisop/test", 11) == 0)
    {
        char *buffer = strdup(buf);
        int start = 0;
        int end = size - 1;
        reverseString(buffer, start, end);
        res = pwrite(fd, buffer, size, offset);
        free(buffer);
    }
    else
    {
        res = pwrite(fd, buf, size, offset);
    }

    if (res == -1)
    {
        res = -errno;
        log_info("write", "Failed to write file content", 0);
    }

    close(fd);

    log_info("write", "Write file content successfully", 1);
    return res;
}


static int xmp_rename(const char *from, const char *to)
{
    char from_path[1000], to_path[1000];
    sprintf(from_path, "%s%s", dirpath, from);
    sprintf(to_path, "%s%s", dirpath, to);

    // Check if the target folder has a prefix "rev"
    if (strncmp(to, "/gallery/rev", 12) == 0)
    {
        // If yes, reverse only the filename (before the last dot, indicating the extension)
        char *filename = strrchr(to_path, '/') + 1;
        char *extension = strrchr(filename, '.');

        if (extension != NULL)
        {
            int start = 0;
            int end = (int)(extension - filename) - 1;
            reverseString(filename, start, end);
        }
    }

    // Check if the target folder has a prefix "delete"
    if (strncmp(to, "/gallery/delete", 15) == 0)
    {
        // If yes, delete the file
        remove(from_path);
        log_info("rename", "File deleted successfully", 1);
        return 0;
    }

    int res = rename(from_path, to_path);

    if (res == -1)
    {
        res = -errno;
        log_info("rename", "Failed to rename file", 0);
    }

    log_info("rename", "File renamed successfully", 1);
    return res;
}



static int xmp_chmod(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = chmod(fpath, mode);

    if (res == -1)
    {
        res = -errno;
        log_info("chmod", "Failed to change file permissions", 0);
    }

    log_info("chmod", "File permissions changed successfully", 1);
    return res;
}



static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .chmod = xmp_chmod,
    .create = xmp_create, // Add create operation
    .open = xmp_open,     // Add open operation
    .write = xmp_write,   // Add write operation
};

int main(int argc, char *argv[])
{
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

2. Namun sayang sekali, ketika saya melakukan run pada program `hell.c`, saya masih belum berhasil dalam menguji programnya, dikarenakan error seperti berikut:


![hell_c_gagal](/uploads/252372e2c1d2758eea93af8e03588cbe/hell_c_gagal.PNG)


Saya sudah melakukan berbagai cara namun errornya masih sama, yaitu `fuse: device not found, try 'modprobe fuse' first`


### **II. SOAL 2<a name="soal2"></a>**
**A. PEMBAHASAN**

Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari

**POIN 1.** Membuat file open-password.c untuk membaca file zip-pass.txt
- Melakukan proses dekripsi base64 terhadap file tersebut
- Lalu unzip home.zip menggunakan password hasil dekripsi

```c
int main() {
    //membaca password terenkripsi dari file zip-pass.txt
    FILE* file = fopen("zip-pass.txt", "r");
    if (file == NULL) {
        printf("Gagal membaca file zip-pass.txt\n");
        return 1;
    }
    
    char encryptedPassword[128];
    if (fgets(encryptedPassword, sizeof(encryptedPassword), file) == NULL) {
        printf("Gagal membaca password terenkripsi\n");
        fclose(file);
        return 1;
    }
    
    fclose(file);
```
Pertama-tama, program membuka file "zip-pass.txt" menggunakan fungsi `fopen` dengan mode "r" (read) untuk membaca isi file. Setelah membuka file, program melakukan pemeriksaan apakah file berhasil dibuka atau tidak. Jika file tidak dapat dibuka (bernilai NULL), program mencetak pesan kesalahan "Gagal membaca file zip-pass.txt" dan mengembalikan nilai 1, mengindikasikan adanya kesalahan.

Selanjutnya, program menggunakan array karakter `encryptedPassword` untuk menyimpan password terenkripsi yang akan dibaca dari file. Fungsi `fgets` digunakan untuk membaca baris pertama dari file dan menyimpannya dalam array `encryptedPassword`. Program kembali melakukan pemeriksaan, kali ini untuk memastikan bahwa pembacaan password terenkripsi berhasil dilakukan. Jika fungsi `fgets` mengembalikan nilai NULL, program mencetak pesan kesalahan "Gagal membaca password terenkripsi", menutup file, dan mengembalikan nilai 1. Setelah berhasil membaca password terenkripsi, program menutup file menggunakan fungsi `fclose`. 

```c
 //mendekripsi password base64
    BIO* bio, *b64;
    char password[128];
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(encryptedPassword, -1);
    bio = BIO_push(b64, bio);
    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
    int decryptedLength = BIO_read(bio, password, sizeof(password));

    if (decryptedLength <= 0) {
        printf("Gagal mendekripsi password base64\n");
        BIO_free_all(bio);
        return 1;
    }
    
    BIO_free_all(bio);
    password[decryptedLength] = '\0'; //menambahkan null-terminator
    
    printf("Password hasil dekripsi: %s\n", password);

  
    return 0;
}
```
Kemudian, dengan menggunakan pustaka OpenSSL, akan didekripsi password yang telah terenkripsi menggunakan algoritma Base64. Dengan menggunakan objek BIO, program menciptakan objek `b64` untuk menangani enkripsi Base64 dan objek `bio` untuk menyimpan password terenkripsi dari memori. Proses dekripsi Base64 dilakukan dengan fungsi `BIO_read`, yang membaca data dari objek `bio` dan menyimpannya dalam array `password`. Setelah dekripsi, program memeriksa keberhasilan proses dengan variabel `decryptedLength`. Jika gagal, program mencetak pesan kesalahan, membebaskan sumber daya objek `bio`, dan mengembalikan nilai 1. Jika berhasil, program membebaskan sumber daya objek `bio`, menambahkan null-terminator pada akhir string password, dan mencetak password hasil dekripsi ke layar.

**POIN 2.** Membuat file semangat.c, dengan ketentuan:
- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format : **[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]**
```c
static const char *log_path = "/home/caca/soal_2/logs-fuse.log"; 

void log_event(const char *tag, const char *path, const char *info, int success) {
    time_t now;
    struct tm *tm_info;
    char timestamp[20];

    time(&now); 
    tm_info = localtime(&now); //mendapatkan waktu saat ini
    strftime(timestamp, 20, "%d/%m/%Y-%H:%M:%S", tm_info); //format waktu

    FILE *log_file = fopen(log_path, "a"); //file log dibuka di mode append
    if (log_file) {
        fprintf(log_file, "[%s]::%s::%s::%s::%s\n", (success ? "SUCCESS" : "FAILED"), timestamp, tag, path, info); //detail pencatatan log
        fclose(log_file); 
    }
}
```
Fungsi `log_event` berfungsi untuk mencatat kegiatan ke file log yang ditentukan (`logs-fuse.log`). Fungsi ini menerima empat parameter: `tag` (sebuah string yang merepresentasikan jenis peristiwa), `path` (path file atau direktori yang terkait dengan peristiwa tersebut), `info` (informasi tambahan atau detail tentang peristiwa), dan `success` (sebuah tanda integer di mana nilai `1` menunjukkan keberhasilan dan `0` menunjukkan kegagalan). Fungsi ini mengambil timestamp saat ini, membuka file log dalam mode "append", dan menulis entri log yang berisi informasi yang diformat, termasuk status keberhasilan, timestamp, tag peristiwa, path terkait, dan informasi tambahan. Terakhir, fungsi menutup file log. 

- Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file sesuai jenisnya:
Hal ini dilakukan dengan embuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka tetapkan:
1. documents: .pdf dan .docx. 
2. images: .jpg, .png, dan .ico. 
3. website: .js, .html, dan .json. 
4. sisop: .c dan .sh. 
5. text: .txt. 
6. ai: .ipynb dan .csv.
```c
static int move_files_to_dir(const char *src_dir, const char *dest_dir, const char *extension, const char *path) { //fungsi untuk memindahkan file
    struct dirent *entry;
    DIR *dir = opendir(src_dir);

    if (dir == NULL) {
        perror("opendir");
        return -1;
    }

    while ((entry = readdir(dir)) != NULL) { //iterasi melalui file pada direktori sumber,
        if (entry->d_type == DT_REG) { //cek jika entry adalah file regular
            const char *file_ext = strrchr(entry->d_name, '.'); //mendapatkan ekstensi file
            if (file_ext && strcmp(file_ext, extension) == 0) { //cocokkan dengan ekstensi yang diinginkan
                char src_path[1000]; 
                char dest_path[1000];
                sprintf(src_path, "%s/%s", src_dir, entry->d_name); //path sumber
                sprintf(dest_path, "%s/%s/%s", dirpath, dest_dir, entry->d_name); //path tujuan

                if (rename(src_path, dest_path) == -1) { //pemindahan file
                    perror("rename");
                    log_event("move_files_to_dir", path, "Failed to move file", 0);
                    return -1;
                }

                printf("File moved: %s -> %s\n", src_path, dest_path);
                log_event("move_files_to_dir", path, "File moved", 1);
            }
        }
    }

    closedir(dir);
    return 0;
}
```
Fungsi `move_files_to_dir` bertujuan untuk memindahkan file dengan ekstensi tertentu dari suatu direktori sumber (`src_dir`) ke direktori tujuan (`dest_dir`). Fungsi ini diawali dengan membuka direktori sumber menggunakan fungsi `opendir`, dan jika operasi tersebut gagal, akan menghasilkan pesan kesalahan dengan `perror` dan mengembalikan nilai `-1`. Selanjutnya, dilakukan iterasi melalui entri-entri file dalam direktori sumber menggunakan loop `while` dan fungsi `readdir`. Pada setiap iterasi, dilakukan pengecekan apakah entri tersebut merupakan file regular (`DT_REG`) dengan menggunakan `entry->d_type`. Jika iya, ekstensi file ditentukan dengan mencari karakter '.' terakhir dalam nama file menggunakan `strrchr`. Kemudian, dilakukan perbandingan antara ekstensi file dengan ekstensi yang diinginkan (`extension`). Jika cocok, path sumber dan path tujuan file yang akan dipindahkan dihasilkan menggunakan fungsi `sprintf`. Selanjutnya, pemindahan file dilakukan dengan fungsi `rename`. Jika operasi ini gagal, pesan kesalahan dicetak menggunakan `perror`, dan fungsi `log_event` dipanggil untuk mencatat kegagalan pemindahan file ke dalam log.

Pada akhirnya, direktori sumber ditutup dengan menggunakan fungsi `closedir`, dan fungsi mengembalikan nilai `0` untuk menunjukkan bahwa pemindahan file telah berhasil. 

```c
static int xmp_mkdir(const char *path, mode_t mode) //fungsi untuk membuat directory dengan path sesuai
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);
    if (res == -1) {
        res = -errno;
        perror("mkdir");
        log_event("mkdir", path, "Failed to create directory", 0);
        return res;
    }

    //memindahkan file dengan format tertentu ke directory yang sesuai
    if (strcmp(path, "/documents") == 0) {
        move_files_to_dir(dirpath, "documents", ".pdf", path);
        move_files_to_dir(dirpath, "documents", ".docx", path);
    } else if (strcmp(path, "/images") == 0) {
        move_files_to_dir(dirpath, "images", ".jpg", path);
        move_files_to_dir(dirpath, "images", ".png", path);
        move_files_to_dir(dirpath, "images", ".ico", path);
    } else if (strcmp(path, "/website") == 0) {
        move_files_to_dir(dirpath, "website", ".js", path);
        move_files_to_dir(dirpath, "website", ".html", path);
        move_files_to_dir(dirpath, "website", ".json", path);
    } else if (strcmp(path, "/sisop") == 0) {
        move_files_to_dir(dirpath, "sisop", ".c", path);
        move_files_to_dir(dirpath, "sisop", ".sh", path);
    } else if (strcmp(path, "/text") == 0) {
        move_files_to_dir(dirpath, "text", ".txt", path);
    } else if (strcmp(path, "/ai") == 0) {
        move_files_to_dir(dirpath, "ai", ".ipynb", path);
        move_files_to_dir(dirpath, "ai", ".csv", path);
    }
    if (res == 0) {
        log_event("mkdir", path, "Directory created", 1);
    }
    return 0;
}
```
Fungsi `xmp_mkdir` digunakan untuk membuat direktori dengan path yang telah ditentukan. Setelah menggabungkan `dirpath` dan `path` untuk mendapatkan path lengkap, fungsi memanggil `mkdir` untuk membuat direktori tersebut dengan mode yang telah ditentukan. Jika pembuatan direktori berhasil, fungsi melakukan pemindahan file dengan ekstensi tertentu ke direktori yang sesuai berdasarkan kategori yang telah ditentukan soal. Pemindahan file ini dilakukan dengan memanggil fungsi `move_files_to_dir`. Selanjutnya, fungsi mencatat keberhasilan pembuatan direktori dalam file log menggunakan `log_event`. Jika terjadi kesalahan selama pembuatan direktori, fungsi mengembalikan nilai `-errno` dan mencatat kegagalan dalam log. Dengan demikian, fungsi ini tidak hanya menciptakan direktori baru, tetapi juga memberikan fungsi pemindahan otomatis berdasarkan kategori file ke direktori yang sesuai.

- Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di dalam folder text maka perlu memasukkan password terlebih dahulu, password terdapat di file password.bin.
```c
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //fungsi untuk ngebaca isi file
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    
    if (strstr(fpath, "/text/") != NULL) { //cek apakah lagi di directory text
        char user_password[100];
        printf("Enter the password: ");
        scanf("%s", user_password);

        //membaca password di file password.bin
        char stored_passwords[1000]; 
        char password_path[1000];
        sprintf(password_path, "%s/password.bin", dirpath);

        FILE *password_file = fopen(password_path, "r");
        if (password_file) {
            fscanf(password_file, "%s", stored_passwords);
            fclose(password_file);

            
            char *stored_passwordd_pass = strtok(storewords, ","); //breakdown line password di password.bin
            while (stored_password != NULL) {
                
                if (strcmp(user_password, stored_password) == 0) { //cek apakah sesuai dengan password di password.bin
                    break; //memperbolehkan akses
                }
                stored_password = strtok(NULL, ",");
            }

            
            if (stored_password == NULL) { //jika password tidak sesuai
                printf("Invalid password\n");
                log_event("read", path, "Invalid password", 0);
                return -EACCES; //melarang akses
            }
        } else {
            printf("Password file not found\n");
            log_event("read", path, "Password file not found", 0);
            return -ENOENT; 
        }
    }
```
Fungsi `xmp_read` dalam sistem berkas FUSE digunakan untuk membaca isi dari suatu file. Pertama, variabel `fpath` dibentuk dengan menggabungkan direktori dasar (`dirpath`) dan path file yang diberikan sebagai argumen. Jika pathnya adalah root ("/"), maka `fpath` diatur menjadi direktori dasar. Selanjutnya, terdapat kondisi yang memeriksa apakah path file berada di dalam direktori "/text/" dengan menggunakan `strstr(fpath, "/text/")`. Jika ya, maka pengguna diminta untuk memasukkan kata sandi dengan menggunakan `scanf`. Dalam konteks ini, fungsi ini mengimplementasikan suatu mekanisme keamanan, dimana akses ke file dalam direktori "/text/" memerlukan autentikasi dengan kata sandi. Kata sandi yang dimasukkan oleh pengguna dibandingkan dengan kata sandi yang tersimpan di file "password.bin" menggunakan fungsi `fopen`, `fscanf`, dan `fclose`.

Selanjutnya, setiap kata sandi yang tersimpan di file "password.bin" dipisahkan menggunakan `strtok` dengan delimiter koma (','). Dilakukan iterasi dengan loop while untuk mencocokkan kata sandi yang dimasukkan oleh pengguna dengan setiap kata sandi yang tersimpan. Jika terdapat kesesuaian, iterasi dihentikan dan akses diberikan. Jika tidak ada kesesuaian, program mencetak pesan "Invalid password", mencatat kegagalan autentikasi ke dalam log menggunakan fungsi `log_event`, dan mengembalikan nilai `-EACCES` untuk melarang akses.

- Pada folder website, membuat file csv pada folder website dengan format ini: file,title,body. Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung tergenerate sebuah file html sejumlah yang telah dibuat pada file csv.
```c
static void generate_html(const char *csv_path) {
    FILE *csv_file = fopen(csv_path, "r"); //membuka file csv
    if (!csv_file) {
        perror("Failed to open CSV file");
        return;
    }

    char line[256];
    int firstLine = 1;  //menandai lagi di baris 1 atau tidak

    while (fgets(line, sizeof(line), csv_file)) {
        if (firstLine) {
            firstLine = 0;
            continue;  //skip baris pertama (format)
        }

        char *file, *title, *body;  //breakdown line untuk baca koma
        file = strtok(line, ",");
        title = strtok(NULL, ",");
        body = strtok(NULL, ",");

        if (file && title && body) {
            //format html
            char html_content[512];
            snprintf(html_content, sizeof(html_content),
                     "<!DOCTYPE html>\n"
                     "<html lang=\"en\">\n"
                     "<head>\n"
                     "<meta charset=\"UTF-8\" />\n"
                     "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n"
                     "<title>%s</title>\n"
                     "</head>\n"
                     "<body>\n"
                     "%s"
                     "</body>\n"
                     "</html>\n",
                     title, body);

            //membuat dan menulis file html ke dalam folder website
            char html_path[1000];
            snprintf(html_path, sizeof(html_path), "%s/website/%s.html", dirpath, file);
            FILE *html_file = fopen(html_path, "w");
            if (html_file) {
                fprintf(html_file, "%s", html_content);
                fclose(html_file);
                printf("HTML file generated: %s\n", html_path);
                log_event("generate_html", html_path, "HTML file generated", 1);
            } else {
                perror("Failed to create HTML file");
                log_event("generate_html", html_path, "Failed to generate HTML file", 0);
            }
        }
    }

    fclose(csv_file);
}
```
Fungsi `generate_html` berfungsi untuk membaca file CSV yang diidentifikasi oleh path `csv_path`, kemudian membuat file HTML berdasarkan isi CSV tersebut. Pertama, fungsi membuka file CSV dengan mode "r" (read) menggunakan `fopen`. Jika file tidak dapat dibuka, fungsi mencetak pesan kesalahan menggunakan `perror` dan mengembalikan kendali. Selanjutnya, fungsi membaca setiap baris dari file CSV menggunakan `fgets`. Baris pertama (header) diabaikan dengan menggunakan variabel `firstLine` yang diperiksa di setiap iterasi. Setiap baris selanjutnya dibagi menjadi tiga bagian menggunakan fungsi `strtok` dengan pemisah koma. Bagian-bagian tersebut mewakili file, judul, dan isi yang digunakan untuk membangun konten HTML. Fungsi menggunakan `snprintf` untuk memformat konten HTML dengan memasukkan judul dan isi ke dalam template HTML yang telah ditentukan.

Selanjutnya, fungsi membentuk path untuk file HTML yang akan dihasilkan di dalam folder "website". Path ini dibentuk menggunakan `snprintf` dengan menggabungkan direktori root (`dirpath`), folder "website", dan nama file HTML. File HTML tersebut kemudian dibuat dan ditulis dengan konten HTML menggunakan `fopen` dan `fprintf`. Jika operasi ini berhasil, fungsi mencetak pesan ke konsol bahwa file HTML telah berhasil dibuat, dan mencatat ke log menggunakan `log_event`. Jika operasi gagal, fungsi mencetak pesan kesalahan dan mencatat ke log bahwa pembuatan file HTML gagal.
```c
static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) { //fungsi untuk menulis file (nano)
    (void) path;

    int res = pwrite(fi->fh, buf, size, offset); //menulis isi file
    if (res == -1) {
        log_event("write", path, "Failed to write to file", 0);
        return -errno;
    }

    char fpath[1000]; //diperiksa apakah lagi di directory website
    sprintf(fpath, "%s%s", dirpath, path);

    if (strstr(path, "/website/") != NULL && strstr(path, ".csv") != NULL) {
        generate_html(fpath); //membuat file html berdasarkan isi csv
    }

    log_event("write", path, "Write to file", 1);
    return res;
}
```
Fungsi `xmp_write` digunakan untuk menangani operasi penulisan pada file. Pertama, fungsi melakukan penulisan isi file menggunakan fungsi `pwrite` dengan argumen seperti file handle (`fi->fh`), buffer (`buf`), ukuran data (`size`), dan offset (`offset`). Hasil penulisan kemudian diperiksa, dan jika terjadi kegagalan, fungsi mencatat pesan ke log menggunakan `log_event`, mengembalikan nilai error yang sesuai.

Selanjutnya, fungsi memeriksa apakah path file berada dalam direktori "/website/" dan memiliki ekstensi ".csv". Jika kondisi tersebut terpenuhi, fungsi memanggil `generate_html(fpath)`. Fungsi `generate_html `bertanggung jawab untuk membaca file CSV, membuat konten HTML berdasarkan isi CSV, dan menyimpannya di dalam folder "website". Setelah itu, fungsi `log_event` digunakan untuk mencatat bahwa penulisan file telah berhasil dilakukan.
```c
static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) { //fungsi untuk bikin file baru
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = open(fpath, fi->flags | O_CREAT, mode);
    if (res == -1) {
        log_event("create", path, "Failed to create file", 0);
        return -errno;
    }

    fi->fh = res;

    log_event("create", path, "Create file", 1);

    if (strstr(path, "/website/") != NULL && strstr(path, ".csv") != NULL) {
        generate_html(fpath); //menampilkan file html berdasarkan isi csv
    }

    return 0;
}
```
Fungsi `xmp_create` digunakan untuk menangani operasi pembuatan file baru. Pertama, fungsi membentuk path lengkap dari file yang akan dibuat menggunakan `sprintf` dengan menggabungkan direktori root (`dirpath`) dan path relatif yang diterima sebagai argumen. Selanjutnya, fungsi menggunakan fungsi `open` untuk membuat file baru dengan memanfaatkan flag `fi->flags | O_CREAT` dan mode yang ditentukan oleh mode_t mode. Hasil dari operasi `open` akan berupa file handle (`fi->fh`), yang kemudian disimpan dalam struktur `fuse_file_info`.

Setelah pembuatan file, fungsi mencatat ke log menggunakan `log_event` bahwa operasi pembuatan file berhasil dilakukan. Selanjutnya, fungsi memeriksa apakah file baru tersebut berada dalam direktori "/website/" dan memiliki ekstensi ".csv". Jika kondisi tersebut terpenuhi, fungsi memanggil `generate_html(fpath)`. `generate_html` bertanggung jawab untuk membaca file CSV, membuat konten HTML berdasarkan isi CSV, dan menyimpannya di dalam folder "website". 


- Tidak dapat menghapus file / folder yang mengandung prefix “restricted”
```c
static int xmp_unlink(const char *path) { //fungsi untuk hapus file (rm)
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    const char *filename = strrchr(path, '/') + 1; //mengambil nama file dari path

    if (strncmp(filename, "restricted", 10) == 0) {  //meriksa apakah nama file dimulai dengan "restricted"
        log_event("unlink", path, "Failed to remove file - Restricted", 0);
        return -EACCES; //akses ditolak untuk file yang dimulai dengan "restricted"
    }

    //lanjutkan dengan penghapusan file jika bukan restricted
    int res = unlink(fpath);
    if (res == -1) {
        log_event("unlink", path, "Failed to remove file", 0);
        return -errno;
    }

    log_event("unlink", path, "Removed file", 1);
    return 0;
}
```
Fungsi `xmp_unlink` digunakan untuk menghapus file dengan path yang diberikan. Pertama, fungsi membentuk path lengkap file dengan menggabungkan `dirpath` (direktori root) dengan `path` yang diberikan. Selanjutnya, fungsi mengambil nama file dari path dengan menggunakan `strrchr` untuk mencari karakter '/' terakhir dan menambahkan 1 untuk melewati karakter '/'. Nama file tersebut disimpan dalam variabel `filename`.

Fungsi selanjutnya melakukan pemeriksaan terhadap nama file. Jika nama file dimulai dengan "restricted" (menggunakan `strncmp`), fungsi mencatat ke log bahwa penghapusan file gagal karena akses ditolak untuk file yang memiliki awalan "restricted", dan mengembalikan nilai `-EACCES` yang menandakan akses ditolak. Jika file bukan memiliki awalan "restricted", fungsi melanjutkan untuk menghapus file dengan menggunakan fungsi `unlink`. Jika penghapusan file berhasil, fungsi mencatat ke log bahwa file telah berhasil dihapus dan mengembalikan nilai `0`.
```c
static int xmp_rmdir(const char *path) { //fungsi untuk menghapus directory
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    const char *dirname = strrchr(path, '/') + 1;  //ambil nama direktori dari path

    if (strncmp(dirname, "restricted", 10) == 0) {
        log_event("rmdir", path, "Failed to remove directory - Restricted", 0);
        return -EACCES; //akses ditolak untuk direktori yang dimulai dengan "restricted"
    }

    int res = rmdir(fpath);
    if (res == -1) {
        log_event("rmdir", path, "Failed to remove directory", 0);
        return -errno;
    }

    log_event("rmdir", path, "Removed directory", 1);
    return 0;
}
```
Selanjutnya, fungsi `xmp_rmdir` digunakan untuk menghapus direktori dengan path yang diberikan. Pertama, fungsi membentuk path lengkap direktori dengan menggabungkan `dirpath` (direktori root) dengan `path` yang diberikan. Selanjutnya, fungsi mengambil nama direktori dari path dengan menggunakan `strrchr` untuk mencari karakter '/' terakhir dan menambahkan 1 untuk melewati karakter '/'. Nama direktori tersebut disimpan dalam variabel `dirname`.

Fungsi selanjutnya melakukan pemeriksaan terhadap nama direktori. Jika nama direktori dimulai dengan "restricted" (menggunakan `strncmp`), fungsi mencatat ke log bahwa penghapusan direktori gagal karena akses ditolak untuk direktori yang memiliki awalan "restricted", dan mengembalikan nilai `-EACCES` yang menandakan akses ditolak. Jika direktori tidak memiliki awalan "restricted", fungsi melanjutkan untuk menghapus direktori dengan menggunakan fungsi `rmdir`. Jika penghapusan direktori berhasil, fungsi mencatat ke log bahwa direktori telah berhasil dihapus dan mengembalikan nilai 0.

- Pada folder documents, karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut
```c
static int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) { //fungsi untuk menetapkan atribut
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = lsetxattr(fpath, name, value, size, flags);

    if (res == -1) {
        log_event("setxattr", path, "Failed to set extended attribute", 0);
        return -errno;
    }

    log_event("setxattr", path, "Set extended attribute", 1);
    return 0;
}
```
Fungsi `xmp_setxattr` bertanggung jawab untuk menetapkan atribut tambahan (extended attribute) pada suatu file atau direktori. Fungsi ini menerima beberapa parameter, termasuk path yang merupakan `path` relatif terhadap root direktori yang ditentukan (`dirpath`). Pertama, fungsi membentuk path lengkap file atau direktori dengan menggabungkan `dirpath` dan path menggunakan `sprintf`, dan menyimpannya dalam variabel `fpath`.

Selanjutnya, fungsi menggunakan sistem call `lsetxattr` untuk menetapkan atribut tambahan pada file atau direktori yang sesuai dengan `fpath`. Fungsi ini menerima beberapa parameter, seperti `name` yang merupakan nama atribut tambahan yang akan di-set, `value` yang berisi nilai atribut, `size` yang menunjukkan ukuran nilai, dan `flags` yang berisi informasi tambahan mengenai operasi atribut (misalnya, `XATTR_CREATE` untuk membuat atribut baru dan mengembalikan galat jika atribut sudah ada). Jika operasi `lsetxattr` berhasil, fungsi mencatat ke log bahwa atribut tambahan telah berhasil di-set dan mengembalikan nilai 0. 
```c
static int xmp_listxattr(const char *path, char *list, size_t size) { //mendaptakan daftar atribut
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = llistxattr(fpath, list, size);

    if (res == -1) {
        log_event("listxattr", path, "Failed to list extended attributes", 0);
        return -errno;
    }

    log_event("listxattr", path, "List extended attributes", 1);
    return res;
}
```
Fungsi `xmp_listxattr` bertanggung jawab untuk mendapatkan daftar atribut tambahan (extended attributes) dari suatu file atau direktori. Fungsi ini menerima beberapa parameter, termasuk `path` yang merupakan path relatif terhadap root direktori yang ditentukan (`dirpath`), serta `list` yang akan diisi dengan daftar atribut dan `size` yang menentukan ukuran dari buffer `list`.

Pertama, fungsi membentuk path lengkap file atau direktori dengan menggabungkan `dirpath` dan `path` menggunakan `sprintf`, dan menyimpannya dalam variabel `fpath`. Selanjutnya, fungsi menggunakan sistem call `llistxattr` untuk mendapatkan daftar atribut tambahan dari file atau direktori yang sesuai dengan `fpath`. Hasilnya disimpan dalam buffer `list` dengan ukuran `size`. Jika operasi `llistxattr` berhasil, fungsi mencatat ke log bahwa daftar atribut tambahan telah berhasil didapatkan dan mengembalikan nilai yang menunjukkan jumlah byte yang telah diisi pada buffer `list`. Jika operasi tersebut gagal, fungsi mencatat ke log bahwa upaya mendapatkan daftar atribut tambahan gagal, dan mengembalikan nilai negatif sesuai dengan kode galat yang dihasilkan oleh sistem (`-errno`). 

Selanjutnya, untuk menjalankan seluruh fungsi FUSE, diinisialisasi operasi fuse.
```c
//inisialisasi operasi fuse
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .open = xmp_open,
    .release = xmp_release,
    .write = xmp_write,
    .create = xmp_create,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmdir,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
};

int main(int argc, char *argv[]) {
    umask(0);

    FILE *log_file = fopen(log_path, "w"); //membuat file log kalau gaada
    if (log_file) {
        fclose(log_file);
    }

    return fuse_main(argc, argv, &xmp_oper, NULL); //mulai fuse
}
```
Program kemudian mendefinisikan sebuah struktur `fuse_operations` yang berisi pointer ke fungsi-fungsi yang akan dipanggil oleh FUSE untuk operasi-operasi tertentu, seperti membaca atribut file, membaca direktori, membaca isi file, dan lainnya. Setiap pointer ini diarahkan ke fungsi yang telah diimplementasikan sebelumnya, seperti `xmp_getattr`, `xmp_readdir`, dan sebagainya.

Dalam fungsi `main`, program pertama-tama melakukan inisialisasi dengan mengatur `umask` ke 0. Selanjutnya, program mencoba membuka file log (dengan path yang telah ditentukan sebelumnya) dalam mode write ("w"). Jika file tersebut belum ada, maka program akan membuatnya. Proses ini berguna untuk memastikan bahwa file log telah ada sebelum program dijalankan. Program akan memanggil fungsi `fuse_main` untuk memulai operasi FUSE. Fungsi ini menerima argumen dari baris perintah (`argc` dan `argv`), pointer ke struktur `fuse_operations` yang telah diinisialisasi sebelumnya (`&xmp_oper`), dan parameter tambahan yang dalam hal ini diatur sebagai `NULL`. Fungsi `fuse_main` akan mengatur dan memulai operasi FUSE sesuai dengan konfigurasi yang telah diberikan. Setelah operasi FUSE selesai, program akan mengembalikan nilai keluaran dari `fuse_main`.

**POIN 3.** Pada folder ai, terdapat file webtoon.csv. Manda diminta untuk membuat sebuah server (socket programming) untuk membaca webtoon.csv. Dimana terjadi pengiriman data antara client ke server dan server ke client. Jadi, pada bagian ini akan dibuat file server.c.
```c
#define PORT 8080

void sendToClient(const char *message, int clientSocket) { //mengirim pesan socker ke client
    send(clientSocket, message, strlen(message), 0);
}
```
Dalam kode ini, terdapat definisi konstanta `PORT` dengan nilai 8080. Fungsi `sendToClient` diimplementasikan untuk mengirim pesan ke client yang terhubung dengan soket tertentu. Parameter pertama merupakan pesan yang akan dikirim, sedangkan parameter kedua adalah soket klien yang menjadi tujuan pengiriman. Fungsi ini menggunakan fungsi sistem `send` untuk mengirim data melalui soket dengan panjang pesan yang ditentukan oleh `strlen(message)`.

- Menampilkan seluruh judul
```c
void sendAllTitles(FILE *file, int clientSocket) { //mengirim semua judul ke client
    char line[256];
    char sendBuffer[1024] = {0};
    int count = 1;

    while (fgets(line, sizeof(line), file)) {
        strtok(line, ","); //memproses bagian pertama (hari)
        strtok(NULL, ","); //memproses bagian kedua (genre)
        char *title = strtok(NULL, ","); //mengambil judul dari line

        char tempBuffer[256] = {0}; //buffer sementara untuk tiap judul
        sprintf(tempBuffer, "%d. %s\n", count, title);
        strcat(sendBuffer, tempBuffer);
        count++;
    }

    sendToClient(sendBuffer, clientSocket); //mengirim judul ke client
}
```
Fungsi `sendAllTitles` ini dirancang untuk membaca data dari suatu file, yang diasumsikan memiliki format CSV (Comma-Separated Values), dan mengirimkan semua judul yang terkandung dalam file tersebut ke klien yang terhubung melalui soket tertentu. Selama iterasi melalui setiap baris file menggunakan `fgets`, fungsi menggunakan fungsi `strtok` untuk memproses bagian pertama dan kedua dari setiap baris (asumsi bahwa file berisi tiga kolom: hari, genre, dan judul). Judul kemudian diambil dari baris tersebut dan disusun dalam format tertentu bersama dengan nomor urutnya, yang kemudian ditambahkan ke buffer sementara `sendBuffer`. Setelah semua judul diproses, buffer tersebut dikirimkan ke klien menggunakan fungsi `sendToClient`. Sebagai tambahan, fungsi ini juga menggunakan variabel `count` untuk memberikan nomor urut pada setiap judul yang dikirim.

- Menampilkan berdasarkan genre
```c
void sendByGenre(const char *genre, FILE *file, int clientSocket) { //mengirim judul berdasarkan genre 
    char line[256];
    char sendBuffer[1024] = {0};
    int count = 1;

    while (fgets(line, sizeof(line), file)) { //membaca tiap baris di file
       
        strtok(line, ","); //melewati nilai koma pertama (hari)
        
    	
        char *genreInFile = strtok(NULL, ","); //ambil genre setelah koma pertama
        char *title = strtok(NULL, ","); //ambil judul

        //menghilangkan spasi
        char formattedGenre[20];
        strncpy(formattedGenre, genre, sizeof(formattedGenre));
        formattedGenre[strcspn(formattedGenre, "\r\n")] = 0; 

        if (strcmp(genreInFile, formattedGenre) == 0) { //jika genre cocok sesuai input
            char tempBuffer[256] = {0};
            sprintf(tempBuffer, "%d. %s\n", count, title); //format nomor dan judul
            strcat(sendBuffer, tempBuffer); //menambahkan judul ke buffer pengiriman
            count++;
        }
    }

    if (count > 1) {
        sendToClient(sendBuffer, clientSocket); //mengirim jduul ke client
    } else {
        sendToClient("No titles found for the given genre.\n", clientSocket);
    }
}
```
Fungsi `sendByGenre` ini bertujuan untuk membaca data dari suatu file dengan format CSV, kemudian mengirimkan judul-judul yang sesuai dengan genre yang diberikan ke klien melalui soket tertentu. Selama iterasi melalui setiap baris file menggunakan `fgets`, fungsi menggunakan `strtok` untuk memproses bagian pertama (hari), kedua (genre), dan ketiga (judul) dari setiap baris. Sebelum membandingkan genre dari file dengan genre yang diberikan sebagai input, fungsi menghilangkan spasi dan karakter newline dari genre input untuk memastikan kecocokan yang tepat. Judul yang sesuai dengan genre tersebut kemudian diformat dengan menambahkan nomor urut dan dimasukkan ke dalam buffer `sendBuffer`. Jika ada judul yang cocok, buffer tersebut dikirimkan ke klien melalui fungsi `sendToClient`, dan jika tidak ada judul yang sesuai, pesan informasi yang sesuai akan dikirimkan.

Jika `count` (variabel yang mencatat jumlah judul yang sesuai) kurang dari atau sama dengan 1, fungsi mengirimkan pesan informasi ke klien bahwa tidak ditemukan judul untuk genre yang diberikan. 

- Menampilkan berdasarkan hari
```c
void sendByDay(const char *day, FILE *file, int clientSocket) { //membaca judul berdasarkan hari
    char line[256];
    char sendBuffer[1024] = {0};
    int count = 1;

    while (fgets(line, sizeof(line), file)) { //membaca tiap baris dalam file
        char *dayInFile = strtok(line, ","); //membaca hari dari baris
        char *genre = strtok(NULL, ","); //lewati bagian genre
        char *title = strtok(NULL, ","); //ambil judul dari baris

        //menghilangkan spasi di awal dan akhir dari string hari
        char formattedDay[20];
        strncpy(formattedDay, day, sizeof(formattedDay));
        formattedDay[strcspn(formattedDay, "\r\n")] = 0; //menghilangkan karakter newline jika ada

        if (strcmp(dayInFile, formattedDay) == 0) { //jika input hari cocok
            char tempBuffer[256] = {0};
            sprintf(tempBuffer, "%d. %s\n", count, title);
            strcat(sendBuffer, tempBuffer); //menambahkan judul ke buffer pengiriman
            count++;
        }
    }

    if (count > 1) {
        sendToClient(sendBuffer, clientSocket); //mengirim judul file
    } else {
        sendToClient("No titles found for the given day.\n", clientSocket);
    }
}
```
Fungsi `sendByDay` ini berfungsi untuk membaca data dari suatu file dengan format CSV, kemudian mengirimkan judul-judul yang terkait dengan hari yang diberikan sebagai input ke klien melalui soket tertentu. Selama iterasi melalui setiap baris file menggunakan `fgets`, fungsi menggunakan `strtok` untuk memproses bagian pertama (hari), kedua (genre), dan ketiga (judul) dari setiap baris. Sebelum membandingkan hari dari file dengan hari yang diberikan sebagai input, fungsi menghilangkan spasi di awal dan akhir string hari untuk memastikan kesesuaian yang akurat. Judul yang sesuai dengan hari tersebut kemudian diformat dengan menambahkan nomor urut dan dimasukkan ke dalam buffer `sendBuffer`. Jika ada judul yang cocok, buffer tersebut dikirimkan ke klien melalui fungsi `sendToClient`, dan jika tidak ada judul yang sesuai, pesan informasi yang sesuai akan dikirimkan.

Jika `count `(variabel yang mencatat jumlah judul yang sesuai) kurang dari atau sama dengan 1, fungsi mengirimkan pesan informasi ke klien bahwa tidak ditemukan judul untuk hari yang diberikan.

- Menambahkan ke dalam file webtoon.csv
```c
void addNewEntry(const char *day, const char *genre, const char *title) { //menambah entry baru ke webtoon.csv
    FILE *file = fopen("webtoon.csv", "a"); //membuka file dalam mode append (tambahan)
    if (file == NULL) {
        printf("Error opening the file.\n");
        return;
    }

    fprintf(file, "%s,%s,%s", day, genre, title); //menulis entry baru
    fclose(file);
}
```
Fungsi `addNewEntry` digunakan untuk menambahkan entri baru ke dalam file "webtoon.csv". Fungsi ini membuka file dalam mode append menggunakan fungsi `fopen`, yang memungkinkan penambahan data ke akhir file. Jika file tidak dapat dibuka, fungsi mencetak pesan kesalahan, dan jika berhasil, fungsi menggunakan `fprintf` untuk menulis baris baru yang berisi informasi hari, genre, dan judul ke file. Terakhir, fungsi menutup file dengan `fclose`. Dengan demikian, fungsi ini memberikan kemampuan untuk menambahkan entri baru ke file CSV yang berisi data tentang webtoon, dengan menerima informasi hari, genre, dan judul sebagai parameter.

- Melakukan delete berdasarkan judul
```c
void deleteByTitle(const char *title) { //menghapus line berdasarkan judul
    FILE *file = fopen("webtoon.csv", "r");
    FILE *tempFile = fopen("temp.csv", "w");

    if (file == NULL || tempFile == NULL) {
        printf("Error opening the file.\n");
        return;
    }

    char line[256];

    while (fgets(line, sizeof(line), file)) { //membaca baris tiap file
        char *copyLine = strdup(line); //duplikasi baris untuk breakdown token
        char *day = strtok(copyLine, ",");
        char *genre = strtok(NULL, ",");
        char *titleToken = strtok(NULL, ",");

        while (isspace(*titleToken)) { //menghilangkan spasi judul
            titleToken++;
        }

        if (strcmp(titleToken, title) != 0) { //jika judul tidak sesuai entry, tulis baris ke file temp
            fprintf(tempFile, "%s", line);
        }
        free(copyLine);
    }

    fclose(file);
    fclose(tempFile);

    remove("webtoon.csv"); //hapus file asli
    rename("temp.csv", "webtoon.csv"); //rename temp jadi webtoon.csv
}
```
Fungsi `deleteByTitle` berguna untuk menghapus baris dalam file "webtoon.csv" yang memiliki judul yang sesuai dengan parameter yang diberikan. Pertama, fungsi membuka file asli (`file`) dalam mode baca ("r") dan file sementara (`tempFile`) dalam mode tulis ("w"). Variabel `line` digunakan untuk menyimpan setiap baris yang dibaca dari file asli. Jika salah satu dari kedua file tidak dapat dibuka, fungsi mencetak pesan kesalahan dan langsung keluar dari fungsi. Dalam proses iterasi, fungsi menggunakan `fgets` untuk membaca setiap baris dari file asli dan kemudian melakukan tokenisasi menggunakan fungsi `strtok` untuk mendapatkan informasi hari, genre, dan judul dari setiap baris. Variabel `copyLine` digunakan untuk membuat duplikat baris yang akan dipecah menjadi token, dan `titleToken` digunakan untuk menyimpan token judul setelah menghilangkan spasi di awalnya.

Selanjutnya, fungsi membandingkan judul dari setiap baris dengan judul yang diberikan sebagai parameter. Jika judul tidak sesuai, baris tersebut ditulis ke dalam file sementara (`tempFile`) menggunakan `fprintf`, sehingga secara efektif menghapus baris yang memiliki judul yang sesuai. Setelah seluruh file asli dibaca dan diproses, kedua file ditutup dengan `fclose`. Langkah selanjutnya adalah menghapus file asli ("webtoon.csv") menggunakan `remove` dan mengubah nama file sementara ("temp.csv") menjadi "webtoon.csv" menggunakan `rename`. Ini dilakukan untuk membuat perubahan yang dihasilkan oleh penghapusan judul menjadi permanen. 

- Penjelasan fungsi main
```c
int main(int argc, char const *argv[]) {
    int server_fd, new_socket;
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) { ///buat socket untuk server
        perror("Socket creation failed");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) { //mengikat socket ke port 
        perror("Binding failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) { //mendengar koneksi dari client
        perror("Listen failed");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) { //menerima koneksi baru
        perror("Accept failed");
        exit(EXIT_FAILURE);
    }
```
Pada fungsi `main`, pertama-tama, sebuah socket server (`server_fd`) dibuat menggunakan fungsi `socket` dengan domain `AF_INET` dan tipe `SOCK_STREAM`. Jika pembuatan socket gagal, program mencetak pesan kesalahan dan keluar dengan status kesalahan. Selanjutnya, struktur `sockaddr_in (address)` diinisialisasi dengan alamat dan port yang ditentukan, dan socket server diikat ke alamat tersebut menggunakan fungsi `bind`. Jika pengikatan gagal, program mencetak pesan kesalahan dan keluar. Selanjutnya, dengan menggunakan fungsi `listen`, server mendengarkan koneksi dari client dengan panjang antrean koneksi maksimum sebanyak 3. Terakhir, server menerima koneksi baru dari client menggunakan fungsi `accept`, dan jika koneksi gagal diterima, program mencetak pesan kesalahan dan keluar. Pada tahap ini, server telah berhasil membuat socket, mengikatnya ke suatu alamat dan port, serta siap menerima koneksi dari client.

```c
FILE *file = fopen("webtoon.csv", "r"); //membuka file webtoon.csv dalam mode baca
    if (file == NULL) {
        printf("Error opening the file.\n");
        return -1;
    }

    while (1) {
        char input[100] = {0};
        int valread = read(new_socket, input, 100);
        printf("Received: %s\n", input);

        if (strstr(input, "hari") != NULL) { //menampilkan judul berdasarkan hari
            char *day = strchr(input, ' ') + 1;
            sendByDay(day, file, new_socket);
        } else if (strstr(input, "genre") != NULL) { //menampilkan judul berdasarkan genre
            char *genre = strchr(input, ' ') + 1;
            sendByGenre(genre, file, new_socket);
        } else if (strstr(input, "all") != NULL) { //menampilkan seluruh judul
            sendAllTitles(file, new_socket);
        } else if (strstr(input, "add") != NULL) { //menambahkan line baru
            sendToClient("Input the day, genre, and title separated by commas (e.g., Monday,Action,Title):\n", new_socket);
            
            char addInfo[256] = {0};
            int valread = read(new_socket, addInfo, 256);
            
            char *day = strtok(addInfo, ",");
            char *genre = strtok(NULL, ",");
            char *title = strtok(NULL, ",");
            
            if (day != NULL && genre != NULL && title != NULL) {
                addNewEntry(day, genre, title);
                sendToClient("New entry added to webtoon.csv\n", new_socket);
        }} else if (strstr(input, "delete") != NULL) { //menghapus line sesuai judul
            sendToClient("Input the title to delete:\n", new_socket);

            char titleToDelete[256] = {0};
            int valread = read(new_socket, titleToDelete, 256);

            deleteByTitle(titleToDelete);
            sendToClient("Entry deleted from webtoon.csv\n", new_socket);
        } else { //jika input client tidak dipahami
            sendToClient("Invalid Command\n", new_socket); 
        }

        fseek(file, 0, SEEK_SET);
    }
 


    fclose(file);
    close(new_socket);
    close(server_fd);

    return 0;
}
```
Selanjutnya, server membuka file "webtoon.csv" dalam mode baca dan menginisialisasi loop utama yang berjalan terus-menerus untuk menanggapi perintah dari klien. Dalam loop ini, server membaca input dari klien menggunakan fungsi `read` pada soket `new_socket`. Berdasarkan input yang diterima, server melakukan berbagai operasi, seperti menampilkan judul berdasarkan kriteria tertentu (`sendByDay`, `sendByGenre`, `sendAllTitles`), menambahkan entri baru (`addNewEntry`), atau menghapus entri berdasarkan judul (`deleteByTitle`).

Selain itu, jika input klien tidak mengandung kata kunci yang sesuai dengan perintah yang diimplementasikan, server memberikan tanggapan kepada klien dengan menggunakan fungsi `sendToClient` dan mengirimkan pesan "Invalid Command" melalui soket `new_socket`. Hal ini memberikan umpan balik kepada klien bahwa perintah yang dimasukkan tidak sesuai dengan perintah yang diharapkan oleh server. Setelah setiap operasi, posisi file direset ke awal agar file dapat dibaca ulang dalam iterasi berikutnya. Setelah selesai melakukan operasi-operasi ini, file ditutup dengan fungsi `fclose`, dan soket klien (`new_socket`) serta soket server (`server_fd`) ditutup menggunakan `close`. 

**B. OUTPUT** 
1. Mengompilasi file open-password.c dan semangat.c

![1-compile_semua](/uploads/886239ea10ced002394ebdca7c113daf/1-compile_semua.jpg)

2. Melakukan unzip untuk files.zip dan home.zip menggunakan password yang didapatkan setelah menjalankan open-password.c menggunakan `./open-password`

![2-unzip_manual___unzip_password](/uploads/ee6314385344d233ce9501a968668c64/2-unzip_manual___unzip_password.jpg)

3. Menjalankan program FUSE pada semangat.c dengan menetapkan mount point menggunakan `./semangat -f mount`

![3-jalanin_mount](/uploads/ae2bb3a1c1275af904933dde10a772f2/3-jalanin_mount.jpg)

4. Pencatatan log

![16-pencatatan_log](/uploads/4e751d2333b8c7bea1d2dd36c0c76333/16-pencatatan_log.jpg)

5. Menjalankan perintah `mkdir` yang akan langsung mengklasifikasikan file

![4-mkdir](/uploads/7992d53f2a83d59cca79e10d47d457d7/4-mkdir.jpg)

6. Menjalankan perintah `cat` pada folder "text" yang akan meminta input password terlebih dahulu


![5-cat_before](/uploads/b335606b583041fbb6b8455442cc3adb/5-cat_before.jpg)

![5-enter_pass](/uploads/b7ab644007418f6d44d6cf27b57fa985/5-enter_pass.jpg)

![5-cat_after](/uploads/9d5a3921d18e84d6dc220ebc268b4260/5-cat_after.jpg)

7. Membuat dan menulis file .csv dengan perintah `nano` pada folder "website" yang akan secara otomatis menghasilkan file .html sesuai yang ditulis di dalam file .csv

![6-csv_html](/uploads/24faa960cf8b4bf1a9daf52cdedf6c25/6-csv_html.jpg)

8. Menjalankan perintah `rm` dan `rmdir` pada file/folder dengan prefix restricted

![7-restricted](/uploads/ca90d9752393b1e02eb0581e4676e96d/7-restricted.jpg)

9. Menjalankan perintah `setfattr` dan `getfattr` pada folder "documents" untuk menetapkan dan menampilkan atribut file

![8-setfattr_getfattr](/uploads/2264789d42f3cec858832ed18f703cd6/8-setfattr_getfattr.jpg)

10. Menginput perintah `all` pada client untuk menampilkan seluruh judul

![9-client_all](/uploads/a93f14fc40e7d40ba09e5559a6093355/9-client_all.jpg)

11. Menginput perintah `hari <nama hari>` pada client untuk menampilkan judul sesuai input hari

![10-client_hari](/uploads/be1d9978ed67ff594b1e6e4cc5325632/10-client_hari.jpg)

12. Menginput perintah `genre <nama genre>` pada client untuk menampilkan judul sesuai input genre

![11-client_genre](/uploads/b59c13526bc762e9c59fe3bfdd4abdc4/11-client_genre.jpg)

13. Menginput perintah `add` pada client untuk menambahkan list hari,genre,judul pada "webtoon.csv"

![12-client_add](/uploads/b8e04e6cd892b0da531408567646a22f/12-client_add.jpg)

![12-webtoon.csv_after_client_add](/uploads/0f3116ba4f064ca06df71e45682d851a/12-webtoon.csv_after_client_add.jpg)

14. Menginput perintah `delete` pada client untuk menghapus list judul pada client sesuai dengan nama judul yang diinput

![13-client_delete](/uploads/6ae2ff88cbe16c91d99ebe85176fb3ed/13-client_delete.jpg)

![13-webtoon.csv_after_client_delete](/uploads/f76d8f8997daa5ecbd97f8e23097deeb/13-webtoon.csv_after_client_delete.jpg)

15. Menginput perintah yang tidak dikenali client dan server

![14-client_invalid_command](/uploads/d9dfe65099a4645e802957f7e120c7b7/14-client_invalid_command.jpg)

16. Hasil pada terminal server untuk setiap input client

![15-server_received](/uploads/9b733a9b910bbd1242fc655f80c1e25e/15-server_received.jpg)

**C. REVISI**  
Tidak ada.


### **III. SOAL 3<a name="soal3"></a>**

**A. PEMBAHASAN**

1. Pertama, direktori asli dan direktori filesystem ditentukan terlebih dahulu. Di sini direktori aslinya adalah /home/adfi/direktoriku , sedangkan direktori filesystem nya /home/adfi/sistemku.

```c
#define FUSE_USE_VERSION 28
#include <dirent.h>
#include <sys/time.h>
#include <sys/xattr.h>
#include <stdbool.h>
#include <unistd.h>
#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#define sz 1024

static const char *logpath = "/home/adfi/fs_module.log";
static const char *dirpath = "/home/adfi/direktoriku";
```

2. Fungsi `split` digunakan untuk membagi file yang berukuran lebih dari sz (1024 bytes) menjadi beberapa bagian kecil. Fungsi ini secara rekursif berjalan melalui direktori dan untuk setiap file regular (bukan direktori), ia memeriksa apakah ukurannya melebihi batas sz. Jika ya, file tersebut dibagi menjadi beberapa bagian kecil, masing-masing dengan ukuran sz. Bagian-bagian tersebut disimpan dengan penamaan yang mencerminkan urutan pemisahan. Setelah pemisahan selesai, file asli dihapus.

```c
void split(char *path) {
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;

    printf("Processing path: %s\n", path);

    while ((entry = readdir(dir))) {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);

        printf("Checking subpath: %s\n", curr);

        if (stat(curr, &sb) == -1) continue;

        char *dirpos = strstr(curr, "/direktoriku/");

        if (S_ISDIR(sb.st_mode)) {
            if (strcmp(entry->d_name, "sistemku") != 0 && (dirpos == NULL || (dirpos != NULL && strstr(curr, "/sistemku/") == NULL))) {
                split(curr);
            }
        } else if (S_ISREG(sb.st_mode)) {
            if (dirpos != NULL) {
                FILE *input = fopen(curr, "rb");
                if (input == NULL) return;

                fseek(input, 0, SEEK_END);
                long fileSize = ftell(input);
                if (fileSize <= sz) continue;
                rewind(input);

                int count = (fileSize + sz - 1) / sz;

                for (int i = 0; i < count; i++) {
                    char chunkNum[5], chunkPath[350];
                    if (i < 10) sprintf(chunkNum, "00%d", i);
                    else if (i < 100) sprintf(chunkNum, "0%d", i);
                    else sprintf(chunkNum, "%d", i);
                    snprintf(chunkPath, sizeof(chunkPath), "%s.%s", curr, chunkNum);

                    FILE *output = fopen(chunkPath, "wb");
                    if (output == NULL) return;

                    char *buffer = (char *)malloc(sz);
                    if (buffer == NULL) return;

                    size_t bytes = fread(buffer, 1, sz, input);
                    fwrite(buffer, 1, bytes, output);
                    free(buffer);
                    fclose(output);

                    printf("File %s split into %s\n", curr, chunkPath);
                }
                fclose(input);

                char command[700];
                snprintf(command, sizeof(command), "rm %s", curr);
                system(command);
            }
        }
    }
    closedir(dir);
}
```

Untuk setiap entri, fungsi memeriksa apakah itu direktori atau file regular. Jika itu adalah direktori dan memenuhi kondisi tertentu (tidak 'sistemku') dan tidak dalam ('/sistemku/'), fungsi akan memanggil dirinya sendiri secara rekursif untuk memproses direktori tersebut. 
    
Jika itu adalah file regular dan berada dalam direktori '/direktoriku/', fungsi akan membagi file menjadi bagian-bagian berukuran sz (1024 bytes) dan menyimpannya dengan penamaan yang mencerminkan urutan pemisahan. Setelah pemisahan selesai, file asli dihapus.

3. Fungsi `merge` digunakan untuk menggabungkan kembali bagian-bagian file yang telah terpisah menjadi file utuh. Fungsi ini bekerja secara rekursif melalui direktori dan untuk setiap file regular, ia mencoba menggabungkan bagian-bagian file tersebut sesuai dengan urutan pemisahan. Setelah penggabungan selesai, bagian-bagian yang terpisah dihapus.

```c
void merge(const char *path){
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;
    while ((entry = readdir(dir))){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);
        if (stat(curr, &sb) == -1) continue;
        if (S_ISDIR(sb.st_mode)){
            merge(curr);
        }

        else if (S_ISREG(sb.st_mode) && strlen(curr) > 3 ){
            int count = 0;
            char dest[300], temp[350];
            memset(dest, '\0', sizeof(dest));
            memset(temp, '\0', sizeof(temp));
            strncpy(dest, curr, strlen(curr) - 4);
            while(1){
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);

                if (stat(temp, &sb)) break;

                printf("File %s merged from %s\n", dest, temp);

                FILE *destfile = fopen(dest, "ab");
                FILE *tempfile = fopen(temp, "rb");
                if (destfile && tempfile) {
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) {
                        fwrite(buffer, 1, read_bytes, destfile);
                    }
                }
                if (tempfile) fclose(tempfile);
                if (destfile) fclose(destfile);
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}
```

4. Fungsi `writelog` digunakan untuk menulis catatan ke file log. Catatan log mencakup jenis log, waktu pencatatan, pemanggilan fungsi, dan argumen yang terkait. Fungsi ini memastikan penulisan catatan log sesuai dengan format yang ditentukan.

```c
void writelog(const char *type, const char *call, const char *arg1, const char *arg2){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);
    char logmsg[1000];
    if (arg2 == NULL){
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1);
    }
    else {
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1, arg2);
    }
    FILE *logfile = fopen(logpath, "a");
    if (logfile != NULL) {
        fprintf(logfile, "%s\n", logmsg);
        fclose(logfile);
    }
}
```

5. Fungsi `fs_create` digunakan untuk membuat file baru. Saat pemanggilan fungsi ini, ia membentuk path lengkap dari direktori tujuan dan nama file. Operasi ini membuka file dengan menggunakan path yang telah dibentuk, dan jika berhasil, catatan log "CREATE" ditulis ke file log. Selain itu pada fungsi ini dibagi menjadi 2 level, yaitu "FLAG" untuk mencatat penghapusan file "UNLINK" dan rmdir direktori "RMDIR" dan "REPORT" digunakan selain operasi yang ada di "FLAG". Fungsi ini mengembalikan file descriptor jika pembuatan file berhasil, atau kode kesalahan jika operasi gagal.

```c
static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = open(fpath, fi->flags, mode);
	writelog("REPORT", "CREATE", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : res;
}
```

6. Fungsi `fs_release` menangani operasi pelepasan file. Ini dipanggil ketika tidak ada lagi referensi ke file yang sedang dibuka. Dalam implementasi ini, catatan log "RELEASE" ditulis ke file log. Fungsi ini mengembalikan nilai 0 sebagai tanda bahwa operasi pelepasan telah berhasil.

```c
static int fs_release(const char *path, struct fuse_file_info *fi){
    writelog("REPORT", "RELEASE", (char*) path, NULL);
    return 0;
}
```

7. Fungsi `fs_getattr` digunakan untuk mendapatkan atribut (metadata) dari suatu file, seperti ukuran, waktu modifikasi, dan jenis file. Fungsi ini membentuk path lengkap dari direktori tujuan dan nama file, dan kemudian menggunakan fungsi `lstat` untuk mendapatkan informasi atribut file tersebut. Catatan log "GETATTR" ditulis ke file log dan fungsi mengembalikan nilai 0 jika operasi berhasil atau kode kesalahan jika gagal.

```c
static int fs_getattr(const char *path, struct stat *stbuf){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    int res = lstat(fpath, stbuf);
    writelog("REPORT", "GETATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
    return (res == -1) ? -errno : 0;
}
```

8. Fungsi `fs_readdir` bertanggung jawab untuk membaca isi dari suatu direktori. Dengan membentuk path lengkap dari direktori tujuan, fungsi membuka direktori dan menggunakan fungsi `readdir` untuk membaca setiap entri. Informasi entri tersebut kemudian diisi ke dalam buffer menggunakan fungsi `filler`. Catatan log "READDIR" ditulis ke file log, dan fungsi ini mengembalikan nilai 0 jika operasi berhasil atau kode kesalahan jika gagal.

```c
static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	DIR *dp;
	struct dirent *de;
	(void) fi;
	(void) offset;
	dp = opendir(fpath);
	if (dp == NULL) return -errno;
	while ((de = readdir(dp))) {
    	struct stat st;
    	memset(&st, 0, sizeof(st));
    	st.st_ino = de->d_ino;
    	st.st_mode = de->d_type << 12;
    	if ((filler(buf, de->d_name, &st, 0)) != 0) break;
	}
	closedir(dp);
	writelog("REPORT", "READDIR", (char*) path, NULL);

	return 0;
}
```

9. Fungsi `fs_read` membaca isi dari suatu file. Dengan membentuk path lengkap dari direktori tujuan dan nama file, fungsi membuka file dengan menggunakan `open` dan membaca isi file menggunakan `pread`. Catatan log "READ" ditulis ke file log, dan fungsi ini mengembalikan jumlah byte yang berhasil dibaca atau kode kesalahan jika gagal.

```c
static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "READ", (char*) path, NULL);
    return res;
}
```

10. Fungsi `fs_rename` digunakan untuk mengganti nama atau memindahkan file. Saat dipanggil, fungsi ini membentuk path lengkap dari file lama (old) dan baru (new). Operasi ini mencoba mengganti nama file menggunakan fungsi `rename`. Catatan log "RENAME" ditulis ke file log, dan jika nama file memenuhi pola tertentu, fungsi `split` atau `merge` dipanggil sesuai kebutuhan. Fungsi ini mengembalikan nilai 0 jika operasi berhasil atau kode kesalahan jika gagal.

```c
static int fs_rename(const char *old, const char *new){
	char oldpath[1000], newpath[1000];
	sprintf(oldpath, "%s%s", dirpath, old);
	sprintf(newpath, "%s%s", dirpath, new);
	int res;
	res = rename(oldpath, newpath);
	if (res == -1) return -errno;
	writelog("REPORT", "RENAME", (char*) old, (char*) new);

	char curr[1000], dup[1000];
	strcpy(curr, dirpath);
	strcpy(dup, new);
	char *token, *oldname = strrchr(old, '/'), *newname = strrchr(new, '/');
	token = strtok(dup, "/");
	while(token != NULL){
    	strcat(curr, "/");
    	strcat(curr, token);
    	struct stat sb;
    	if (stat(curr, &sb) == -1) continue;
    	if (strlen(token) >= 7 && !strncmp(token, "module_", 7) && S_ISDIR(sb.st_mode)){
        	split(curr);
        	break;
    	}
    	token = strtok(NULL, "/");
	}

	if (strcmp(oldname, newname)){
    	struct stat sb;
    	if ((stat(newpath, &sb) != -1) && S_ISDIR(sb.st_mode) && (strlen(newname) < 8 || strncmp(newname, "/module_", 8))) merge(newpath);
	}

	return 0;
}
```

11. Fungsi `fs_unlink` digunakan untuk menghapus file. Dengan membentuk path lengkap dari direktori tujuan dan nama file, fungsi ini mencoba menghapus file menggunakan fungsi `unlink`. Catatan log "UNLINK" ditulis ke file log, dan fungsi ini mengembalikan nilai 0 jika operasi berhasil atau kode kesalahan jika gagal.

```c
static int fs_unlink(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = unlink(fpath);
    writelog("FLAG", "UNLINK", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

12. Fungsi `fs_rmdir` digunakan untuk menghapus direktori. Saat dipanggil, fungsi ini membentuk path lengkap dari direktori tujuan. Operasi ini mencoba menghapus direktori menggunakan fungsi `rmdir`. Catatan log "RMDIR" ditulis ke file log, dan fungsi ini mengembalikan nilai 0 jika operasi berhasil atau kode kesalahan jika gagal.

```c
static int fs_rmdir(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = rmdir(fpath);
	writelog("FLAG", "RMDIR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

13. Fungsi `fs_mkdir` digunakan untuk membuat direktori baru. Dengan membentuk path lengkap dari direktori tujuan, fungsi ini mencoba membuat direktori baru menggunakan fungsi `mkdir`. Catatan log "MKNOD" ditulis ke file log, dan fungsi ini mengembalikan nilai 0 jika operasi berhasil atau kode kesalahan jika gagal.

```c
static int fs_mkdir(const char *path, mode_t mode){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = mkdir(fpath, mode);
	writelog("REPORT", "MKNOD", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}
```

14. Fungsi `fs_open` membuka file untuk dibaca atau ditulis. Dengan membentuk path lengkap dari direktori tujuan dan nama file, fungsi ini mencoba membuka file menggunakan fungsi `open`. Catatan log "OPEN" ditulis ke file log, dan fungsi ini mengembalikan nilai 0 jika operasi berhasil atau kode kesalahan jika gagal.

```c
static int fs_open(const char *path, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = open(fpath, fi->flags);
    if (res == -1) return -errno;
    close(res);
	writelog("REPORT", "OPEN", (char*) path, NULL);
    return 0;
}
```

15. Fungsi `fs_write` digunakan untuk menulis ke file. Dengan membentuk path lengkap dari direktori tujuan dan nama file, fungsi ini membuka file untuk ditulis menggunakan `open`. Isi yang akan ditulis diambil dari buffer menggunakan `pwrite`. Catatan log "WRITE" ditulis ke file log, dan fungsi ini mengembalikan jumlah byte yang berhasil ditulis atau kode kesalahan jika gagal.

```c
static int fs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_WRONLY);
    if (fd == -1) return -errno;

    int res = pwrite(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "WRITE", (char*) path, NULL);
    return res;
}
```

16. Pada `fuse_operations` yang digunakan oleh FUSE (Filesystem in Userspace) untuk menetapkan fungsi-fungsi operasi file sistem yang akan diimplementasikan oleh file sistem FUSE yang Anda buat. Ini memberikan peta antara operasi file sistem dan fungsi-fungsi yang akan dipanggil ketika operasi tersebut dijalankan. 

```c
static  struct fuse_operations fs_oper = {
	.getattr = fs_getattr,
	.readdir = fs_readdir,
	.mkdir = fs_mkdir,
	.unlink = fs_unlink,
	.rmdir = fs_rmdir,
	.rename = fs_rename,
	.open = fs_open,
	.read = fs_read,
	.write = fs_write,
	.create = fs_create,
	.release = fs_release
};
```

Struktur ini memberikan koneksi antara fungsi-fungsi implementasi file sistem dengan operasi-operasi file sistem standar yang dipicu oleh FUSE. Dengan menetapkan fungsi-fungsi ini, memberikan perilaku khusus untuk setiap operasi tertentu yang dapat diakses melalui sistem file FUSE

17. Fungsi `main` adalah titik masuk utama program. Ini menetapkan mode umask ke 0 dan memastikan bahwa direktori yang diperlukan `/home/adfi` dan `/home/adfi/sistemku` dapat diakses. Selanjutnya, program mencetak pesan bahwa sistem file FUSE sedang dimulai. Setelah itu, fungsi `fuse_main` dipanggil untuk menjalankan sistem file FUSE dengan operasi yang didefinisikan di `fs_oper`. Setelah selesai, program mencetak status keluaran dari sistem file FUSE.

```c
int main(int  argc, char *argv[]){
	umask(0);
     if (access("/home/adfi", F_OK) == -1) {
        perror("Error accessing home directory");
        return EXIT_FAILURE;
    }

    if (access("/home/adfi/sistemku", F_OK) == -1) {
        perror("Error accessing sistemku directory");
        return EXIT_FAILURE;
    }

    printf("Starting FUSE filesystem...\n");

    int fuse_status = fuse_main(argc, argv, &fs_oper, NULL);

    printf("FUSE filesystem exited with status: %d\n", fuse_status);

    return fuse_status;
}
```

**B. OUTPUT** 
1. Modularisasi pada direktori asli (split)

![1modulardir](/uploads/340c91cfa4da0fb80eef748d0ad21424/1modulardir.png)

2. Modularisasi pada direktori filesystem (split)

![2modularsistem](/uploads/89e5374b287b0478b408ed80e40082eb/2modularsistem.png)

3. Unmodularisasi pada direktori asli dan filesystem (merge)

![3rename_merge](/uploads/af135226c0f07f4aeb37a77699d7f5b6/3rename_merge.png)

4. Hasil `fs_module.log`

![4log1](/uploads/5ca5ddda0f05131f82fd18a5325162b5/4log1.png)

![5log2](/uploads/a2cab459b6fa922ea0ba2d7f292dda6a/5log2.png)

5. Bisa membaca file pada direktori filesystem

![7bisamembacafile](/uploads/e43dfd3e4f37ad9fd097a65f79a61796/7bisamembacafile.png)

6. Pesan Debugging

![8pesandebugging](/uploads/efb01a7cbd7b1e1c2148cd8651b1a916/8pesandebugging.png)


**C. REVISI** 

1. Memperbaiki agar program bisa melakukan split dan merge.

- PROGRAM SEBELUM DIREVISI

```c

#define USER_NAME "adfi"
#define LOG_FILE_PATH "/home/" USER_NAME "/fs_module.log"

#define ORIGINAL_DIR_PATH "/home/adfi/dir_asli"

#define LEVEL_REPORT "REPORT"
#define LEVEL_FLAG "FLAG"

void modularize_file(const char *path);
void unmodularize_file(const char *path);

int is_modular_dir(const char *path) {
    return (strncmp(path, "/home/adfi/sistemku/module_", 27) == 0);
}

char *get_file_name(const char *path) {
    char *filename = strdup(path);
    char *dot = strrchr(filename, '.');
    if (dot != NULL) {
        *dot = '\0';
    }
    return filename;
}

void modularize_and_split_file(const char *path) {
    FILE *original_file = fopen(path, "rb");

    if (original_file != NULL) {
        char *filename = get_file_name(path);
        char buffer[1024];
        int count = 0;

        char modular_dir[256];
        snprintf(modular_dir, sizeof(modular_dir), "/home/%s/dir_asli/module_", USER_NAME);
        mkdir(modular_dir, 0755);

        printf("Modularizing file: %s\n", path);

        while (!feof(original_file)) {
            size_t bytesRead = fread(buffer, 1, sizeof(buffer), original_file);

            if (bytesRead > 0) {
                char new_filename[512];
                snprintf(new_filename, sizeof(new_filename), "%s/%s.%03d", modular_dir, filename, count);

                FILE *modular_file = fopen(new_filename, "wb");
                if (modular_file != NULL) {
                    fwrite(buffer, 1, bytesRead, modular_file);
                    fclose(modular_file);
                } else {
                    perror("Error creating modular file");
                    break;
                }

                count++;
            }
        }

        fclose(original_file);
        free(filename);

        printf("File split successfully: %s\n", path);
    } else {
        perror("Error opening original file");
    }
}

void modularize_dir(const char *path) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL) {
        perror("Error opening directory");
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char entry_path[257];
        snprintf(entry_path, sizeof(entry_path), "%s/%s", path, entry->d_name);

        struct stat entry_stat;
        if (lstat(entry_path, &entry_stat) == 0) {
            if (S_ISREG(entry_stat.st_mode)) {
                modularize_and_split_file(entry_path);
            } else if (S_ISDIR(entry_stat.st_mode)) {
                modularize_dir(entry_path);
            }
        } else {
            perror("Error getting file/directory information");
        }
    }

    closedir(dir);
}

void unmodularize_dir(const char *path) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(path);
    if (dir == NULL) {
        perror("Error opening directory");
        return;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        char entry_path[257];
        snprintf(entry_path, sizeof(entry_path), "%s/%s", path, entry->d_name);

        struct stat entry_stat;
        if (lstat(entry_path, &entry_stat) == 0) {
            if (S_ISREG(entry_stat.st_mode)) {
                unmodularize_file(entry_path);
            } else if (S_ISDIR(entry_stat.st_mode)) {
                unmodularize_dir(entry_path);
            }
        } else {
            perror("Error getting file/directory information");
        }
    }

    closedir(dir);
}

void modularize_file(const char *path) {
    char *filename = get_file_name(path);
    char buffer[1024];
    FILE *original_file = fopen(path, "rb");

    if (original_file != NULL) {
        int count = 0;
        size_t bytesRead;

        char modular_dir[256];
        snprintf(modular_dir, sizeof(modular_dir), "/home/%s/dir_asli/module_", USER_NAME);
        mkdir(modular_dir, 0755);

        while ((bytesRead = fread(buffer, 1, sizeof(buffer), original_file)) > 0) {
            char new_filename[512];
            snprintf(new_filename, sizeof(new_filename), "%s/%s.%03d", modular_dir, filename, count);
            FILE *modular_file = fopen(new_filename, "wb");
            if (modular_file != NULL) {
                fwrite(buffer, 1, bytesRead, modular_file);
                fclose(modular_file);
            } else {
                perror("Error creating modular file");
                break;
            }
            count++;
        }

        fclose(original_file);
    } else {
        perror("Error opening original file");
    }

    free(filename);
}

void unmodularize_file(const char *path) {
    char *filename = get_file_name(path);
    char buffer[1024];

    FILE *original_file = fopen(path, "wb");

    if (original_file != NULL) {
        int count = 0;
        char modular_filename[512];

        char modular_dir[256];
        snprintf(modular_dir, sizeof(modular_dir), "/home/%s/dir_asli/module_", USER_NAME);

        snprintf(modular_filename, sizeof(modular_filename), "%s/%s.%03d", modular_dir, filename, count);
        FILE *modular_file = fopen(modular_filename, "rb");

        while (modular_file != NULL) {
            size_t bytesRead = fread(buffer, 1, sizeof(buffer), modular_file);
            if (bytesRead > 0) {
                fwrite(buffer, 1, bytesRead, original_file);
            } else {
                fclose(modular_file);
                count++;
                snprintf(modular_filename, sizeof(modular_filename), "%s/%s.%03d", modular_dir, filename, count);
                modular_file = fopen(modular_filename, "rb");
            }
        }

        fclose(original_file);
    }

    free(filename);
}


static int fs_getattr(const char *path, struct stat *stbuf) {
    printf("fs_getattr called for path: %s\n", path);
    int res = 0;

    memset(stbuf, 0, sizeof(struct stat));

    if (strcmp(path, "/") == 0) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else if (is_modular_dir(path)) {
        stbuf->st_mode = S_IFDIR | 0755;
        stbuf->st_nlink = 2;
    } else {
        char full_path[256];
        snprintf(full_path, sizeof(full_path), "%s%s", ORIGINAL_DIR_PATH, path);

        res = lstat(full_path, stbuf);
        if (res == -1) {
            return -errno;
        }

        if (is_modular_dir(full_path)) {
            stbuf->st_size = 1024;
        }
    }

    return res;
}


static int fs_mkdir(const char *path, mode_t mode) {
    char full_path[256];
    snprintf(full_path, sizeof(full_path), "%s%s", ORIGINAL_DIR_PATH, path);

    int res = mkdir(full_path, mode);
    if (res == 0) {
        log_operation(LEVEL_REPORT, "MKDIR", NULL, full_path);
    } else {
        perror("Error creating directory");
        log_operation(LEVEL_REPORT, "MKDIR", strerror(errno), full_path);
    }

    return res;
}

static int fs_rename(const char *from, const char *to) {
    char full_from[256];
    char full_to[256];
    snprintf(full_from, sizeof(full_from), "%s%s", ORIGINAL_DIR_PATH, from);
    snprintf(full_to, sizeof(full_to), "%s%s", ORIGINAL_DIR_PATH, to);

    int res = rename(full_from, full_to);
    if (res == 0) {
        if (strncmp(from, "/sistemku/module_", 18) == 0 &&
            strncmp(to, "/sistemku/module_", 18) != 0) {
            unmodularize_dir(full_to);

            log_operation(LEVEL_REPORT, "RENAME", from, to);
        } else if (strncmp(from, "/sistemku/module_", 18) != 0 &&
                   strncmp(to, "/sistemku/module_", 18) == 0) {
            modularize_and_split_file(full_to);

            log_operation(LEVEL_REPORT, "RENAME", from, to);
        } else {
            log_operation(LEVEL_REPORT, "RENAME", from, to);
        }
    } else {
        perror("Error renaming");
    }

    return res;
}


static int fs_rmdir(const char *path) {
    char full_path[256];
    snprintf(full_path, sizeof(full_path), "%s%s", ORIGINAL_DIR_PATH, path);

    if (strncmp(path, "/sistemku/module_", 18) == 0) {
        unmodularize_dir(full_path);
    }

    int res = rmdir(full_path);
    if (res == 0) {
        log_operation(LEVEL_FLAG, "RMDIR", path, NULL);
    } else {
        log_operation(LEVEL_REPORT, "RMDIR", strerror(errno), path);
        perror("Error removing directory");
    }

    return res;
}

static int fs_unlink(const char *path) {
    char full_path[256];
    snprintf(full_path, sizeof(full_path), "%s%s", ORIGINAL_DIR_PATH, path);

    int res = unlink(full_path);
    if (res == 0) {
        log_operation(LEVEL_FLAG, "UNLINK", path, NULL);
    } else {
        log_operation(LEVEL_REPORT, "UNLINK", strerror(errno), path);
        perror("Error unlinking file");
    }

    return res;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    if (strncmp(path, "/sistemku/module_", 18) == 0) {
        unmodularize_file(path);
    }

    (void)offset;
    (void)fi;

    char full_path[256];
    snprintf(full_path, sizeof(full_path), "%s%s", ORIGINAL_DIR_PATH, path);

    DIR *dir;
    struct dirent *entry;

    dir = opendir(full_path);
    if (dir == NULL) {
        perror("Error opening directory");
        return -errno;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        if (filler(buf, entry->d_name, NULL, 0) != 0) {
            closedir(dir);
            return -ENOMEM;
        }
    }

    closedir(dir);

    printf("Contents of %s:\n", full_path);
    dir = opendir(full_path);
    while ((entry = readdir(dir)) != NULL) {
        printf("%s\n", entry->d_name);
    }
    closedir(dir);

    return 0;
}

static int fs_open(const char *path, struct fuse_file_info *fi) {
    if (is_modular_dir(path)) {
        unmodularize_dir(path);
    }

    int res = 0;

    if (strcmp(path, "/fs_module.log") != 0) {
        return -ENOENT;
    }

    if ((fi->flags & O_ACCMODE) != O_RDONLY) {
        return -EACCES;
    }

    return res;
}

static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    if (is_modular_dir(path)) {
        unmodularize_file(path);
    }

    if (strcmp(path, "/fs_module.log") != 0) {
        return -ENOENT;
    }

    FILE *log_file = fopen(LOG_FILE_PATH, "r");
    if (log_file == NULL) {
        perror("Error opening log file for reading");
        return -EIO;
    }

    if (fseek(log_file, offset, SEEK_SET) == -1) {
        perror("Error seeking log file");
        fclose(log_file);
        return -EIO;
    }

    size_t bytesRead = fread(buf, 1, size, log_file);
    if (bytesRead == 0 && !feof(log_file)) {
        perror("Error reading from log file");
        fclose(log_file);
        return -EIO;
    }

    fclose(log_file);

    return bytesRead;
}


static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char full_path[256];
    snprintf(full_path, sizeof(full_path), "/home/%s%s", USER_NAME, path);

    int fd = open(full_path, fi->flags, mode);
    if (fd == -1) {
        perror("Error creating file");
        return -errno;
    }

    close(fd);

    log_operation(LEVEL_REPORT, "CREATE", NULL, full_path);

    return 0;
}


```

    Saya merombak ulang beberapa kodenya untuk memperbaiki kembali algoritmanya agar bisa melakukan split dan merge file modularnya. Berikut kode hasil revisi:

- PROGRAM SETELAH DIREVISI

```c
#define sz 1024

static const char *logpath = "/home/adfi/fs_module.log";
static const char *dirpath = "/home/adfi/direktoriku";

void split(char *path) {
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;

    printf("Processing path: %s\n", path);

    while ((entry = readdir(dir))) {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);

        printf("Checking subpath: %s\n", curr);

        if (stat(curr, &sb) == -1) continue;

        char *dirpos = strstr(curr, "/direktoriku/");

        if (S_ISDIR(sb.st_mode)) {
            if (strcmp(entry->d_name, "sistemku") != 0 && (dirpos == NULL || (dirpos != NULL && strstr(curr, "/sistemku/") == NULL))) {
                split(curr);
            }
        } else if (S_ISREG(sb.st_mode)) {
            if (dirpos != NULL) {
                FILE *input = fopen(curr, "rb");
                if (input == NULL) return;

                fseek(input, 0, SEEK_END);
                long fileSize = ftell(input);
                if (fileSize <= sz) continue;
                rewind(input);

                int count = (fileSize + sz - 1) / sz;

                for (int i = 0; i < count; i++) {
                    char chunkNum[5], chunkPath[350];
                    if (i < 10) sprintf(chunkNum, "00%d", i);
                    else if (i < 100) sprintf(chunkNum, "0%d", i);
                    else sprintf(chunkNum, "%d", i);
                    snprintf(chunkPath, sizeof(chunkPath), "%s.%s", curr, chunkNum);

                    FILE *output = fopen(chunkPath, "wb");
                    if (output == NULL) return;

                    char *buffer = (char *)malloc(sz);
                    if (buffer == NULL) return;

                    size_t bytes = fread(buffer, 1, sz, input);
                    fwrite(buffer, 1, bytes, output);
                    free(buffer);
                    fclose(output);

                    printf("File %s split into %s\n", curr, chunkPath);
                }
                fclose(input);

                char command[700];
                snprintf(command, sizeof(command), "rm %s", curr);
                system(command);
            }
        }
    }
    closedir(dir);
}

void merge(const char *path){
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;
    while ((entry = readdir(dir))){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);
        if (stat(curr, &sb) == -1) continue;
        if (S_ISDIR(sb.st_mode)){
            merge(curr);
        }

        else if (S_ISREG(sb.st_mode) && strlen(curr) > 3 ){
            int count = 0;
            char dest[300], temp[350];
            memset(dest, '\0', sizeof(dest));
            memset(temp, '\0', sizeof(temp));
            strncpy(dest, curr, strlen(curr) - 4);
            while(1){
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);

                if (stat(temp, &sb)) break;

                printf("File %s merged from %s\n", dest, temp);

                FILE *destfile = fopen(dest, "ab");
                FILE *tempfile = fopen(temp, "rb");
                if (destfile && tempfile) {
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) {
                        fwrite(buffer, 1, read_bytes, destfile);
                    }
                }
                if (tempfile) fclose(tempfile);
                if (destfile) fclose(destfile);
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}

static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = open(fpath, fi->flags, mode);
	writelog("REPORT", "CREATE", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : res;
}

static int fs_release(const char *path, struct fuse_file_info *fi){
    writelog("REPORT", "RELEASE", (char*) path, NULL);
    return 0;
}

static int fs_getattr(const char *path, struct stat *stbuf){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    int res = lstat(fpath, stbuf);
    writelog("REPORT", "GETATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
    return (res == -1) ? -errno : 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	DIR *dp;
	struct dirent *de;
	(void) fi;
	(void) offset;
	dp = opendir(fpath);
	if (dp == NULL) return -errno;
	while ((de = readdir(dp))) {
    	struct stat st;
    	memset(&st, 0, sizeof(st));
    	st.st_ino = de->d_ino;
    	st.st_mode = de->d_type << 12;
    	if ((filler(buf, de->d_name, &st, 0)) != 0) break;
	}
	closedir(dp);
	writelog("REPORT", "READDIR", (char*) path, NULL);

	return 0;
}

static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "READ", (char*) path, NULL);
    return res;
}

static int fs_rename(const char *old, const char *new){
	char oldpath[1000], newpath[1000];
	sprintf(oldpath, "%s%s", dirpath, old);
	sprintf(newpath, "%s%s", dirpath, new);
	int res;
	res = rename(oldpath, newpath);
	if (res == -1) return -errno;
	writelog("REPORT", "RENAME", (char*) old, (char*) new);

	char curr[1000], dup[1000];
	strcpy(curr, dirpath);
	strcpy(dup, new);
	char *token, *oldname = strrchr(old, '/'), *newname = strrchr(new, '/');
	token = strtok(dup, "/");
	while(token != NULL){
    	strcat(curr, "/");
    	strcat(curr, token);
    	struct stat sb;
    	if (stat(curr, &sb) == -1) continue;
    	if (strlen(token) >= 7 && !strncmp(token, "module_", 7) && S_ISDIR(sb.st_mode)){
        	split(curr);
        	break;
    	}
    	token = strtok(NULL, "/");
	}

	if (strcmp(oldname, newname)){
    	struct stat sb;
    	if ((stat(newpath, &sb) != -1) && S_ISDIR(sb.st_mode) && (strlen(newname) < 8 || strncmp(newname, "/module_", 8))) merge(newpath);
	}

	return 0;
}

static int fs_unlink(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = unlink(fpath);
    writelog("FLAG", "UNLINK", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

static int fs_rmdir(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = rmdir(fpath);
	writelog("FLAG", "RMDIR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

static int fs_mkdir(const char *path, mode_t mode){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = mkdir(fpath, mode);
	writelog("REPORT", "MKNOD", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

static int fs_open(const char *path, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = open(fpath, fi->flags);
    if (res == -1) return -errno;
    close(res);
	writelog("REPORT", "OPEN", (char*) path, NULL);
    return 0;
}

static int fs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_WRONLY);
    if (fd == -1) return -errno;

    int res = pwrite(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "WRITE", (char*) path, NULL);
    return res;
}
```

2. Memperbaiki agar dapat membuka isi file pada direktori filesystem. Berikut hasil revisinya.

- SEBELUM DIREVISI

```c
static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    if (is_modular_dir(path)) {
        unmodularize_file(path);
    }

    if (strcmp(path, "/fs_module.log") != 0) {
        return -ENOENT;
    }

    FILE *log_file = fopen(LOG_FILE_PATH, "r");
    if (log_file == NULL) {
        perror("Error opening log file for reading");
        return -EIO;
    }

    if (fseek(log_file, offset, SEEK_SET) == -1) {
        perror("Error seeking log file");
        fclose(log_file);
        return -EIO;
    }

    size_t bytesRead = fread(buf, 1, size, log_file);
    if (bytesRead == 0 && !feof(log_file)) {
        perror("Error reading from log file");
        fclose(log_file);
        return -EIO;
    }

    fclose(log_file);

    return bytesRead;
}
```

- SETELAH DIREVISI

```c
static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "READ", (char*) path, NULL);
    return res;
}
```
