#define FUSE_USE_VERSION 28
#include <dirent.h>
#include <sys/time.h>
#include <sys/xattr.h>
#include <stdbool.h>
#include <unistd.h>
#include <fuse.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#define sz 1024

static const char *logpath = "/home/adfi/fs_module.log";
static const char *dirpath = "/home/adfi/direktoriku";

void split(char *path) {
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;

    printf("Processing path: %s\n", path);

    while ((entry = readdir(dir))) {
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);

        printf("Checking subpath: %s\n", curr);

        if (stat(curr, &sb) == -1) continue;

        char *dirpos = strstr(curr, "/direktoriku/");

        if (S_ISDIR(sb.st_mode)) {
            if (strcmp(entry->d_name, "sistemku") != 0 && (dirpos == NULL || (dirpos != NULL && strstr(curr, "/sistemku/") == NULL))) {
                split(curr);
            }
        } else if (S_ISREG(sb.st_mode)) {
            if (dirpos != NULL) {
                FILE *input = fopen(curr, "rb");
                if (input == NULL) return;

                fseek(input, 0, SEEK_END);
                long fileSize = ftell(input);
                if (fileSize <= sz) continue;
                rewind(input);

                int count = (fileSize + sz - 1) / sz;

                for (int i = 0; i < count; i++) {
                    char chunkNum[5], chunkPath[350];
                    if (i < 10) sprintf(chunkNum, "00%d", i);
                    else if (i < 100) sprintf(chunkNum, "0%d", i);
                    else sprintf(chunkNum, "%d", i);
                    snprintf(chunkPath, sizeof(chunkPath), "%s.%s", curr, chunkNum);

                    FILE *output = fopen(chunkPath, "wb");
                    if (output == NULL) return;

                    char *buffer = (char *)malloc(sz);
                    if (buffer == NULL) return;

                    size_t bytes = fread(buffer, 1, sz, input);
                    fwrite(buffer, 1, bytes, output);
                    free(buffer);
                    fclose(output);

                    printf("File %s split into %s\n", curr, chunkPath);
                }
                fclose(input);

                char command[700];
                snprintf(command, sizeof(command), "rm %s", curr);
                system(command);
            }
        }
    }
    closedir(dir);
}

void merge(const char *path){
    DIR *dir = opendir(path);
    struct dirent *entry;
    struct stat sb;
    while ((entry = readdir(dir))){
        if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
        char curr[300];
        snprintf(curr, sizeof(curr), "%s/%s", path, entry->d_name);
        if (stat(curr, &sb) == -1) continue;
        if (S_ISDIR(sb.st_mode)){
            merge(curr);
        }

        else if (S_ISREG(sb.st_mode) && strlen(curr) > 3 ){
            int count = 0;
            char dest[300], temp[350];
            memset(dest, '\0', sizeof(dest));
            memset(temp, '\0', sizeof(temp));
            strncpy(dest, curr, strlen(curr) - 4);
            while(1){
                if (count < 10) snprintf(temp, sizeof(temp), "%s.00%d", dest, count);
                else if (count < 100) snprintf(temp, sizeof(temp), "%s.0%d", dest, count);
                else snprintf(temp, sizeof(temp), "%s.%d", dest, count);

                if (stat(temp, &sb)) break;

                printf("File %s merged from %s\n", dest, temp);

                FILE *destfile = fopen(dest, "ab");
                FILE *tempfile = fopen(temp, "rb");
                if (destfile && tempfile) {
                    char buffer[1024];
                    size_t read_bytes;
                    while ((read_bytes = fread(buffer, 1, sizeof(buffer), tempfile)) > 0) {
                        fwrite(buffer, 1, read_bytes, destfile);
                    }
                }
                if (tempfile) fclose(tempfile);
                if (destfile) fclose(destfile);
                remove(temp);
                count++;
            }
        }
    }
    closedir(dir);
}

void writelog(const char *type, const char *call, const char *arg1, const char *arg2){
    time_t t = time(NULL);
    struct tm *curr = localtime(&t);
    char logmsg[1000];
    if (arg2 == NULL){
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1);
    }
    else {
        snprintf(logmsg, sizeof(logmsg), "%s::%02d%02d%02d-%02d:%02d:%02d::%s::%s::%s", type, (curr->tm_year + 1900) % 100, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec, call, arg1, arg2);
    }
    FILE *logfile = fopen(logpath, "a");
    if (logfile != NULL) {
        fprintf(logfile, "%s\n", logmsg);
        fclose(logfile);
    }
}

static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = open(fpath, fi->flags, mode);
	writelog("REPORT", "CREATE", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : res;
}

static int fs_release(const char *path, struct fuse_file_info *fi){
    writelog("REPORT", "RELEASE", (char*) path, NULL);
    return 0;
}

static int fs_getattr(const char *path, struct stat *stbuf){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);
    int res = lstat(fpath, stbuf);
    writelog("REPORT", "GETATTR", (char*) path, (res == -1) ? strerror(errno) : NULL);
    return (res == -1) ? -errno : 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	DIR *dp;
	struct dirent *de;
	(void) fi;
	(void) offset;
	dp = opendir(fpath);
	if (dp == NULL) return -errno;
	while ((de = readdir(dp))) {
    	struct stat st;
    	memset(&st, 0, sizeof(st));
    	st.st_ino = de->d_ino;
    	st.st_mode = de->d_type << 12;
    	if ((filler(buf, de->d_name, &st, 0)) != 0) break;
	}
	closedir(dp);
	writelog("REPORT", "READDIR", (char*) path, NULL);

	return 0;
}

static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_RDONLY);
    if (fd == -1) return -errno;

    int res = pread(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "READ", (char*) path, NULL);
    return res;
}

static int fs_rename(const char *old, const char *new){
	char oldpath[1000], newpath[1000];
	sprintf(oldpath, "%s%s", dirpath, old);
	sprintf(newpath, "%s%s", dirpath, new);
	int res;
	res = rename(oldpath, newpath);
	if (res == -1) return -errno;
	writelog("REPORT", "RENAME", (char*) old, (char*) new);

	char curr[1000], dup[1000];
	strcpy(curr, dirpath);
	strcpy(dup, new);
	char *token, *oldname = strrchr(old, '/'), *newname = strrchr(new, '/');
	token = strtok(dup, "/");
	while(token != NULL){
    	strcat(curr, "/");
    	strcat(curr, token);
    	struct stat sb;
    	if (stat(curr, &sb) == -1) continue;
    	if (strlen(token) >= 7 && !strncmp(token, "module_", 7) && S_ISDIR(sb.st_mode)){
        	split(curr);
        	break;
    	}
    	token = strtok(NULL, "/");
	}

	if (strcmp(oldname, newname)){
    	struct stat sb;
    	if ((stat(newpath, &sb) != -1) && S_ISDIR(sb.st_mode) && (strlen(newname) < 8 || strncmp(newname, "/module_", 8))) merge(newpath);
	}

	return 0;
}

static int fs_unlink(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = unlink(fpath);
    writelog("FLAG", "UNLINK", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

static int fs_rmdir(const char *path){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = rmdir(fpath);
	writelog("FLAG", "RMDIR", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

static int fs_mkdir(const char *path, mode_t mode){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
	int res = mkdir(fpath, mode);
	writelog("REPORT", "MKNOD", (char*) path, (res == -1) ? strerror(errno) : NULL);
	return (res == -1) ? -errno : 0;
}

static int fs_open(const char *path, struct fuse_file_info *fi){
	char fpath[1000];
	sprintf(fpath, "%s%s", dirpath, path);
    int res = open(fpath, fi->flags);
    if (res == -1) return -errno;
    close(res);
	writelog("REPORT", "OPEN", (char*) path, NULL);
    return 0;
}

static int fs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, O_WRONLY);
    if (fd == -1) return -errno;

    int res = pwrite(fd, buf, size, offset);
    if (res == -1) res = -errno;

    close(fd);
    writelog("REPORT", "WRITE", (char*) path, NULL);
    return res;
}


static  struct fuse_operations fs_oper = {
	.getattr = fs_getattr,
	.readdir = fs_readdir,
	.mkdir = fs_mkdir,
	.unlink = fs_unlink,
	.rmdir = fs_rmdir,
	.rename = fs_rename,
	.open = fs_open,
	.read = fs_read,
	.write = fs_write,
	.create = fs_create,
	.release = fs_release
};

int main(int  argc, char *argv[]){
	umask(0);
     if (access("/home/adfi", F_OK) == -1) {
        perror("Error accessing home directory");
        return EXIT_FAILURE;
    }

    if (access("/home/adfi/sistemku", F_OK) == -1) {
        perror("Error accessing sistemku directory");
        return EXIT_FAILURE;
    }

    printf("Starting FUSE filesystem...\n");

    int fuse_status = fuse_main(argc, argv, &fs_oper, NULL);

    printf("FUSE filesystem exited with status: %d\n", fuse_status);

    return fuse_status;
}
